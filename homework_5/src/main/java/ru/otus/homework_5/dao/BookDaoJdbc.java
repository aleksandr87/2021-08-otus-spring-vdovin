package ru.otus.homework_5.dao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.otus.homework_5.domain.Author;
import ru.otus.homework_5.domain.Book;
import ru.otus.homework_5.domain.Genre;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class BookDaoJdbc implements BookDao {

    private final NamedParameterJdbcOperations jdbc;

    BookDaoJdbc(NamedParameterJdbcOperations namedParameterJdbcOperations) {
        this.jdbc = namedParameterJdbcOperations;
    }

    public long insert(Book book) {
        KeyHolder kh = new GeneratedKeyHolder();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("title", book.getTitle());
        params.addValue("genreId", book.getGenre().getId());
        jdbc.update("insert into books (title, genre_id) values (:title, :genreId)", params, kh);
        if (!book.getAuthors().isEmpty()) {
            for (Author author : book.getAuthors()) {
                params.addValue("authorId", author.getId());
                params.addValue("id", kh.getKey().longValue());
                jdbc.update("insert into book_author (book_id, author_id) values (:id, :authorId)", params);
            }
        }
        return kh.getKey().longValue();
    }

    @Override
    public Book getById(long id) {
        Map<String, Object> params = Collections.singletonMap("id", id);
        return jdbc.query(
                "SELECT b.id as bookid, b.title, a.id as authorid, a.name as authorname, g.id as genreid, g.name as genrename  FROM BOOKS b left join BOOK_AUTHOR ba on b.ID = ba.book_id left join AUTHORS a on ba.author_id = a.id left join GENRES g on b.genre_id = g.id WHERE b.id = :id",
                params, new BookMapper()).get(0);
    }

    @Override
    public List<Book> getAll() {
        return jdbc.getJdbcOperations().query(
                "SELECT b.id as bookid, b.title, a.id as authorid, a.name as authorname, g.id as genreid, g.name as genrename  FROM BOOKS b left join BOOK_AUTHOR ba on b.ID = ba.book_id left join AUTHORS a on ba.author_id = a.id left join GENRES g on b.genre_id = g.id",
                new BookMapper()
        );
    }

    @Override
    public int update(Book book) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", book.getId());
        params.put("title", book.getTitle());
        params.put("genreId", book.getGenre().getId());
        int result = jdbc.update("update books set title = :title, genre_id = :genreId where id = :id", params);
        if(result > 0) {
            jdbc.update("delete from book_author where book_id = :id", params);
            if (!book.getAuthors().isEmpty()) {
                Map<String, Long>[] paramsArr = new HashMap[book.getAuthors().size()];
                int count = 0;
                for (Author author : book.getAuthors()) {
                    HashMap<String, Long> map = new HashMap<>();
                    map.put("id", book.getId());
                    map.put("authorId", author.getId());
                    paramsArr[count++] = map;
                }
                jdbc.batchUpdate("insert into book_author (book_id, author_id) values (:id, :authorId)", paramsArr);
            }
        }
        return result;
    }

    @Override
    public int deleteById(long id) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        jdbc.update("delete from book_author where book_id = :id", params);
        return jdbc.update("delete from books where id = :id", params);
    }

    private static class BookMapper implements ResultSetExtractor<List<Book>> {
        @Override
        public List<Book> extractData(ResultSet rs) throws SQLException, DataAccessException {
            if (rs.isBeforeFirst()) {
                Map<Long, Book> books = new HashMap<>();
                while (rs.next()) {
                    Long id = rs.getLong("bookid");
                    if(!books.containsKey(id)) {
                        Genre genre = new Genre();
                        genre.setId(rs.getLong("genreid"));
                        genre.setName(rs.getString("genrename"));
                        Author author = new Author();
                        author.setId(rs.getLong("authorid"));
                        author.setName(rs.getString("authorname"));
                        Set<Author> authorSet = new HashSet<>();
                        authorSet.add(author);
                        Book book = new Book();
                        book.setId(id);
                        book.setTitle(rs.getString("title"));
                        book.setGenre(genre);
                        book.setAuthors(authorSet);
                        books.put(id, book);
                    } else {
                        Author author = new Author();
                        author.setId(rs.getLong("authorid"));
                        author.setName(rs.getString("authorname"));
                        Book book = books.get(id);
                        book.getAuthors().add(author);
                    }
                }
                return new ArrayList<>(books.values());
            } else {
                throw new DataAccessException("Requested book doesn't exist."){};
            }
        }
    }
}
