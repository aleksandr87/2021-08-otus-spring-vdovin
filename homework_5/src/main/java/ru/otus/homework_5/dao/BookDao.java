package ru.otus.homework_5.dao;

import ru.otus.homework_5.domain.Book;

import java.util.List;

public interface BookDao {

    long insert(Book book);

    Book getById(long id);

    List<Book> getAll();

    int update(Book book);

    int deleteById(long id);
}
