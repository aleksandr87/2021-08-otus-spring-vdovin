package ru.otus.homework_5.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.otus.homework_5.domain.Author;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Map;

@Repository
public class AuthorDaoJdbc implements AuthorDao {

    private final NamedParameterJdbcOperations jdbc;

    AuthorDaoJdbc(NamedParameterJdbcOperations namedParameterJdbcOperations) {
        this.jdbc = namedParameterJdbcOperations;
    }

    public long insert(Author author) {
        KeyHolder kh = new GeneratedKeyHolder();
        SqlParameterSource params = new MapSqlParameterSource().addValue("name", author.getName());
        jdbc.update("insert into authors (name) values (:name)", params, kh);
        return kh.getKey().longValue();
    };

    public Author getById(long id) {
        Map<String, Object> params = Collections.singletonMap("id", id);
        return jdbc.queryForObject("select * from authors where id = :id", params, new AuthorMapper());
    }

    public Author getByName(String name) {
        Map<String, Object> params = Collections.singletonMap("name", name);
        return jdbc.queryForObject("select * from authors where name = :name", params, new AuthorMapper());
    }

    @Override
    public int update(Author author) {
        Map<String, Object> params = Map.of("id", author.getId(), "name", author.getName());
        return jdbc.update("update authors set name = :name where id = :id", params);
    }

    @Override
    public int deleteById(long id) {
        Map<String, Object> params = Collections.singletonMap("id", id);
        jdbc.update("delete from book_author where author_id = :id", params);
        return jdbc.update("delete from authors where id = :id", params);
    }

    private static class AuthorMapper implements RowMapper<Author> {
        @Override
        public Author mapRow(ResultSet rs, int rowNum) throws SQLException {
            long id = rs.getLong("id");
            String name = rs.getString("name");
            return new Author(id, name);
        }
    }
}
