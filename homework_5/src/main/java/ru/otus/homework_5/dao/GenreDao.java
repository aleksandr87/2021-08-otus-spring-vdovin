package ru.otus.homework_5.dao;

import ru.otus.homework_5.domain.Genre;

public interface GenreDao {

    long insert(Genre genre);

    Genre getById(long id);

    Genre getByName(String name);

    int update(Genre genre);
}
