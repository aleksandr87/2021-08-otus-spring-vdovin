package ru.otus.homework_5.dao;

import ru.otus.homework_5.domain.Author;

public interface AuthorDao {

    long insert(Author author);

    Author getById(long id);

    Author getByName(String name);

    int update(Author author);

    int deleteById(long id);
}
