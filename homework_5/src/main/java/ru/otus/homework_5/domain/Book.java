package ru.otus.homework_5.domain;

import java.util.Set;

public class Book {

    private long id;
    private String title;
    private Set<Author> authors;
    private Genre genre;

    public Book(long id, String title, Set<Author> authors, Genre genre) {
        this.id = id;
        this.title = title;
        this.authors = authors;
        this.genre = genre;
    }
    public Book() {}

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public String toString() {
        return String.format("ID: %s; TITLE: %s; AUTHORS: %s; GENRE: %s", id, title, authors.stream().map(Author::getName).reduce((String n1, String n2) -> n1+", "+n2).get(), genre.getName());
    }
}
