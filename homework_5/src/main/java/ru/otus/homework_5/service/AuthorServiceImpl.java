package ru.otus.homework_5.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import ru.otus.homework_5.dao.AuthorDao;
import ru.otus.homework_5.domain.Author;
import ru.otus.homework_5.exception.DataSourceException;

@Service
public class AuthorServiceImpl implements AuthorService {

    private final AuthorDao dao;

    @Autowired
    public AuthorServiceImpl(AuthorDao dao) {
        this.dao = dao;
    }

    @Override
    public long createAuthor(String name) throws DataSourceException {
        Author author = new Author();
        author.setName(name);
        try {
            return dao.insert(author);
        } catch (DataAccessException dae) {
            throw new DataSourceException(DataSourceException.AUTHOR_ALREADY_EXISTS, name);
        }
    }

    @Override
    public Author getAuthor(long id) throws DataSourceException {
        try {
            return dao.getById(id);
        } catch (DataAccessException dae) {
            throw new DataSourceException(DataSourceException.AUTHOR_ID_NOT_EXISTS, String.valueOf(id));
        }
    }

    @Override
    public Author getAuthor(String name) throws DataSourceException {
        try {
            return dao.getByName(name);
        } catch (DataAccessException dae) {
            throw new DataSourceException(DataSourceException.AUTHOR_NAME_NOT_EXISTS, name);
        }
    }

    @Override
    public int updateAuthor(long id, String authorName) {
        Author author = new Author(id, authorName);
        return dao.update(author);
    }

    @Override
    public int deleteAuthor(long id) {
        return dao.deleteById(id);
    }
}
