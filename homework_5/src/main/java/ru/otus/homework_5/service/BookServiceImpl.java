package ru.otus.homework_5.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import ru.otus.homework_5.dao.BookDao;
import ru.otus.homework_5.domain.Author;
import ru.otus.homework_5.domain.Book;
import ru.otus.homework_5.domain.Genre;
import ru.otus.homework_5.exception.DataSourceException;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class BookServiceImpl implements BookService {

    private final BookDao bookDao;
    private final AuthorService authorService;
    private final GenreService genreService;

    @Autowired
    public BookServiceImpl(BookDao bookDao, AuthorService authorService, GenreService genreService) {
        this.bookDao = bookDao;
        this.authorService = authorService;
        this.genreService = genreService;
    }

    @Override
    public long createBook(String title, String genre, String[] authors) {
        Book bookToCreate = new Book();
        bookToCreate.setTitle(title);
        if(genre != null) {
            long genreId = 0;
            try {
                genreId = genreService.getGenre(genre).getId();
            } catch(DataSourceException dse) {
                System.out.println(dse.getMessage());
            }
            if (genreId == 0) {
                try {
                    genreId = genreService.createGenre(genre);
                    System.out.println(String.format("Genre with name %s was created, id: %d", genre, genreId));
                } catch (DataSourceException dse) {
                    dse.getMessage();
                }
            }
            bookToCreate.setGenre(new Genre(genreId, genre));
        }
        if(authors.length > 0) {
            Set<Author> authorSet = new HashSet<>();
            for(String authorName: authors) {
                long authorId = 0;
                try {
                    authorId = authorService.getAuthor(authorName).getId();
                } catch(DataSourceException dse) {
                    System.out.println(dse.getMessage());
                }
                if (authorId == 0) {
                    try {
                        authorId = authorService.createAuthor(authorName);
                        System.out.println(String.format("Author with name %s was created, id: %d", authorName, authorId));
                    } catch (DataSourceException dse) {
                        dse.getMessage();
                    }
                }
                authorSet.add(new Author(authorId, authorName));
            }
            bookToCreate.setAuthors(authorSet);
        }
        return bookDao.insert(bookToCreate);
    }

    @Override
    public Book getBook(long id) throws DataSourceException {
        try {
            return bookDao.getById(id);
        } catch (DataAccessException dse) {
            throw new DataSourceException(DataSourceException.BOOK_ID_NOT_EXISTS, String.valueOf(id));
        }
    }

    @Override
    public List<Book> getAllBooks() {
        return bookDao.getAll();
    }

    @Override
    public int updateBook(long id, String title, String genre, String[] authors) {
        Book bookToUpdate = new Book();
        bookToUpdate.setId(id);
        bookToUpdate.setTitle(title);
        if(genre != null) {
            long genreId = 0;
            try {
                genreId = genreService.getGenre(genre).getId();
            } catch(DataSourceException dse) {
                System.out.println(dse.getMessage());
            }
            if (genreId == 0) {
                try {
                    genreId = genreService.createGenre(genre);
                    System.out.println(String.format("Genre with name %s was created, id: %d", genre, genreId));
                } catch (DataSourceException dse) {
                    dse.getMessage();
                }
            }
            bookToUpdate.setGenre(new Genre(genreId, genre));
        }
        if(authors.length > 0) {
            Set<Author> authorSet = new HashSet<>();
            for(String authorName: authors) {
                long authorId = 0;
                try {
                    authorId = authorService.getAuthor(authorName).getId();
                } catch(DataSourceException dse) {
                    System.out.println(dse.getMessage());
                }
                if (authorId == 0) {
                    try {
                        authorId = authorService.createAuthor(authorName);
                        System.out.println(String.format("Author with name %s was created, id: %d", authorName, authorId));
                    } catch (DataSourceException dse) {
                        dse.getMessage();
                    }
                }
                authorSet.add(new Author(authorId, authorName));
            }
            bookToUpdate.setAuthors(authorSet);
        }
        return bookDao.update(bookToUpdate);
    }

    @Override
    public int deleteBook(long id) {
        return bookDao.deleteById(id);
    }
}
