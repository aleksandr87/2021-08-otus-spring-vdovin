package ru.otus.homework_5.service;

import ru.otus.homework_5.domain.Author;
import ru.otus.homework_5.exception.DataSourceException;

public interface AuthorService {
    long createAuthor(String name) throws DataSourceException;

    Author getAuthor(long id) throws DataSourceException;

    Author getAuthor(String name) throws DataSourceException;

    int updateAuthor(long id, String authorName);

    int deleteAuthor(long id);
}
