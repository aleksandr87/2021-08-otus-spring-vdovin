package ru.otus.homework_5.service;

import ru.otus.homework_5.domain.Book;
import ru.otus.homework_5.exception.DataSourceException;

import java.util.List;

public interface BookService {

    long createBook(String title, String genre, String[] authors);

    Book getBook(long id) throws DataSourceException;

    List<Book> getAllBooks();

    int updateBook(long id, String title, String genre, String[] authors);

    int deleteBook(long id);
}
