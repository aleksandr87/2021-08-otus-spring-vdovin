package ru.otus.homework_5.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import ru.otus.homework_5.dao.GenreDao;
import ru.otus.homework_5.domain.Genre;
import ru.otus.homework_5.exception.DataSourceException;

@Service
public class GenreServiceImpl implements GenreService {

    private final GenreDao dao;

    @Autowired
    public GenreServiceImpl(GenreDao dao) {
        this.dao = dao;
    }

    @Override
    public long createGenre(String name) throws DataSourceException {
        Genre genre = new Genre();
        genre.setName(name);
        try {
            return dao.insert(genre);
        } catch (DataAccessException dae) {
            throw new DataSourceException(DataSourceException.GENRE_ALREADY_EXISTS, name);
        }
    }

    @Override
    public Genre getGenre(long id) throws DataSourceException {
        try {
            return dao.getById(id);
        } catch(DataAccessException dae) {
            throw new DataSourceException(DataSourceException.GENRE_ID_NOT_EXISTS, String.valueOf(id));
        }
    }

    @Override
    public Genre getGenre(String name) throws DataSourceException {
        try {
            return dao.getByName(name);
        } catch(DataAccessException dae) {
            throw new DataSourceException(DataSourceException.GENRE_NAME_NOT_EXISTS, name);
        }
    }

    @Override
    public int updateGenre(long id, String genreName) {
        Genre genre = new Genre(id, genreName);
        return dao.update(genre);
    }

}
