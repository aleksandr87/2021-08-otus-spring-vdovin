package ru.otus.homework_5.service;

import ru.otus.homework_5.domain.Genre;
import ru.otus.homework_5.exception.DataSourceException;

public interface GenreService {
    long createGenre(String name) throws DataSourceException;

    Genre getGenre(long id) throws DataSourceException;

    Genre getGenre(String name) throws DataSourceException;

    int updateGenre(long id, String genreName);
}
