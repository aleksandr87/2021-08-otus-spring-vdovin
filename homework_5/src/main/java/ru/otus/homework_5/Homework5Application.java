package ru.otus.homework_5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Homework5Application {

    public static void main(String[] args) throws Exception {
        ApplicationContext context = SpringApplication.run(Homework5Application.class);
    }
}
