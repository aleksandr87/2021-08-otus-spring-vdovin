package ru.otus.homework_5.shell;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import ru.otus.homework_5.exception.DataSourceException;
import ru.otus.homework_5.service.AuthorService;
import ru.otus.homework_5.service.BookService;
import ru.otus.homework_5.service.GenreService;

import java.util.List;
import java.util.stream.Collectors;

@ShellComponent
public class ShellExecutor {

    private final AuthorService authorService;
    private final BookService bookService;
    private final GenreService genreService;

    public ShellExecutor(AuthorService authorService, BookService bookService, GenreService genreService) {
        this.authorService = authorService;
        this.bookService = bookService;
        this.genreService = genreService;
    }

    @ShellMethod(value = "get book", key = {"gb", "get book"})
    public String getBook(@ShellOption("--id") long id) {
        try {
            return "Requested book: " + bookService.getBook(id).toString();
        } catch(DataSourceException dae) {
            return dae.getMessage();
        }
    }

    @ShellMethod(value = "get all books", key = {"gab", "get all books"})
    public List<String> getAllBooks() {
        return bookService.getAllBooks().stream().map(book -> book.toString()).collect(Collectors.toList());
    }

    @ShellMethod(value = "create book", key = {"cb", "create book"})
    public String createBook(@ShellOption("--title") String title, @ShellOption("--genre") String genre, @ShellOption(value = "--authors") String authors) {
        String[] authorNames = authors.split(";");
        long id = bookService.createBook(title, genre, authorNames);
        return String.format("Book with title %s was created, id: %d", title, id);
    }

    @ShellMethod(value = "update book", key = {"ub", "update book"})
    public String updateBook(@ShellOption("--id") long id, @ShellOption("--title") String title, @ShellOption("--genre") String genre, @ShellOption(value = "--authors") String authors) {
        String[] authorNames = authors.split(";");
        int result = bookService.updateBook(id, title, genre, authorNames);
        if (result == 1) {
            return String.format("Book with ID %d was updated.", id);
        } else {
            return String.format("Book with ID %d doesn't exist.", id);
        }
    }

    @ShellMethod(value = "delete book", key = {"db", "delete book"})
    public String deleteBook(@ShellOption("--id") long id) {
        int result = bookService.deleteBook(id);
        if (result == 1) {
            return String.format("Book with ID %d was deleted.", id);
        } else {
            return String.format("Book with ID %d doesn't exist.", id);
        }
    }

    @ShellMethod(value = "get author", key = {"ga", "get author"})
    public String getAuthor(@ShellOption("--id") long id) {
        try {
            return "Requested author: " + authorService.getAuthor(id).toString();
        } catch(DataSourceException dae) {
            return dae.getMessage();
        }
    }

    @ShellMethod(value = "create author", key = {"ca", "create author"})
    public String createAuthor(@ShellOption("--name") String name) {
        try {
            long id = authorService.createAuthor(name);
            return String.format("Author with name %s was created, id: %d", name, id);
        } catch (DataSourceException dse) {
            return dse.getMessage();
        }
    }

    @ShellMethod(value = "update author", key = {"ua", "update author"})
    public String updateAuthor(@ShellOption("--id") long id, @ShellOption("--name") String name) {
        int result = authorService.updateAuthor(id, name);
        if (result == 1) {
            return String.format("Author with id %d was updated", id);
        } else {
            return String.format("Author with id %d was not updated", id);
        }
    }

    @ShellMethod(value = "delete author", key = {"da", "delete author"})
    public String deleteAuthor(@ShellOption("--id") long id) {
        int result = authorService.deleteAuthor(id);
        if (result == 1) {
            return String.format("Author with ID %d was deleted.", id);
        } else {
            return String.format("Author with ID %d doesn't exist.", id);
        }
    }

    @ShellMethod(value = "get genre", key = {"gg", "get genre"})
    public String getGenre(@ShellOption("--id") long id) {
        try {
            return "Requested genre: " + genreService.getGenre(id).toString();
        } catch(DataSourceException dae) {
            return dae.getMessage();
        }
    }

    @ShellMethod(value = "create genre", key = {"cg", "create genre"})
    public String createGenre(@ShellOption("--name") String name) {
        try {
            long id = genreService.createGenre(name);
            return String.format("Genre with name %s was created, id: %d", name, id);
        } catch (DataSourceException dse) {
            return dse.getMessage();
        }
    }

    @ShellMethod(value = "update genre", key = {"ug", "update genre"})
    public String updateGenre(@ShellOption("--id") long id, @ShellOption("--name") String name) {
        int result = genreService.updateGenre(id, name);
        if (result == 1) {
            return String.format("Genre with id %d was updated", id);
        } else {
            return String.format("Genre with id %d was not updated", id);
        }
    }
}
