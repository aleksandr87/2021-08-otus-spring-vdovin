package ru.otus.homework_5.exception;

public class DataSourceException extends Exception {

    public static final String AUTHOR_NAME_NOT_EXISTS = "Author with name %s doesn't exist.";
    public static final String AUTHOR_ID_NOT_EXISTS = "Author with ID %s doesn't exist.";
    public static final String AUTHOR_ALREADY_EXISTS = "Author with name %s already exists.";
    public static final String GENRE_NAME_NOT_EXISTS = "Genre with name %s doesn't exist.";
    public static final String GENRE_ID_NOT_EXISTS = "Genre with ID %s doesn't exist.";
    public static final String GENRE_ALREADY_EXISTS = "Genre with name %s already exists.";
    public static final String BOOK_ID_NOT_EXISTS = "Book with ID %s doesn't exist.";

    private final String errorMessage;
    private final String queryParam;

    public DataSourceException(String errorMessage, String queryParam) {
        this.errorMessage = errorMessage;
        this.queryParam = queryParam;
    }

    public String getMessage() {
        return String.format(errorMessage, queryParam);
    }
}
