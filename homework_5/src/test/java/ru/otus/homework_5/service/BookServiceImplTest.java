package ru.otus.homework_5.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.otus.homework_5.dao.BookDao;
import ru.otus.homework_5.domain.Author;
import ru.otus.homework_5.domain.Book;
import ru.otus.homework_5.domain.Genre;
import ru.otus.homework_5.exception.DataSourceException;

import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;


@SpringBootTest
public class BookServiceImplTest {

    private static final long BOOK_ID = 1L;
    private static final String BOOK_TITLE = "Title";
    private static final String BOOK_NEW_TITLE = "New title";
    private static final long GENRE_ID = 1L;
    private static final String GENRE_NAME = "Genre";
    private static final long AUTHOR_ID = 1L;
    private static final String AUTHOR_NAME = "Author";
    private static final int BOOK_LIST_SIZE = 1;

    @Autowired
    private BookServiceImpl bookService;

    @MockBean
    private BookDao bookDao;

    private Book testBook;

    @BeforeEach
    void setUp() {
        Genre testGenre = new Genre(GENRE_ID, GENRE_NAME);
        Author testAuthor = new Author(AUTHOR_ID, AUTHOR_NAME);
        Set<Author> testAuthorSet = Set.of(testAuthor);
        testBook = new Book(BOOK_ID, BOOK_TITLE, testAuthorSet, testGenre);
    }

    @Test
    public void createBookTest() {
        given(bookDao.insert(any())).willReturn(BOOK_ID);
        long id = bookService.createBook(BOOK_TITLE, GENRE_NAME, new String[]{AUTHOR_NAME});
        assertThat(id).isEqualTo(BOOK_ID);
    }

    @Test
    public void getBookTest() throws DataSourceException {
        given(bookDao.getById(anyLong())).willReturn(testBook);
        Book expectedBook = bookService.getBook(BOOK_ID);
        assertThat(expectedBook).usingRecursiveComparison().isEqualTo(testBook);
    }

    @Test
    public void getAllBooksTest() {
        given(bookDao.getAll()).willReturn(List.of(testBook));
        List<Book> expectedBookList = bookService.getAllBooks();
        assertThat(expectedBookList).hasSize(BOOK_LIST_SIZE).contains(testBook);
    }

    @Test
    public void updateBookTest() {
        int rowsUpdated = 1;
        given(bookDao.update(any())).willReturn(rowsUpdated);
        int result = bookService.updateBook(BOOK_ID, BOOK_NEW_TITLE, GENRE_NAME, new String[]{AUTHOR_NAME});
        assertThat(result).isEqualTo(rowsUpdated);
    }

    @Test
    public void deleteBookTest() {
        int rowsDeleted = 1;
        given(bookDao.deleteById(anyLong())).willReturn(rowsDeleted);
        int result = bookService.deleteBook(BOOK_ID);
        assertThat(result).isEqualTo(rowsDeleted);
    }
}
