package ru.otus.homework_5.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.Import;
import org.springframework.dao.DataAccessException;
import ru.otus.homework_5.domain.Author;
import ru.otus.homework_5.domain.Book;
import ru.otus.homework_5.domain.Genre;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@JdbcTest
@Import({BookDaoJdbc.class, GenreDaoJdbc.class, AuthorDaoJdbc.class})
public class BookDaoJdbcTest {

    private static final long EXISTING_BOOK_ID = 100L;
    private static final String EXISTING_BOOK_TITLE = "Title";
    private static final long EXISTING_GENRE_ID = 100L;
    private static final String EXISTING_GENRE_NAME = "Genre";
    private static final long EXISTING_AUTHOR1_ID = 100L;
    private static final String EXISTING_AUTHOR1_NAME = "Author 1";
    private static final long EXISTING_AUTHOR2_ID = 101L;
    private static final String EXISTING_AUTHOR2_NAME = "Author 2";
    private static final int MIN_BOOKS_AMOUNT = 1;

    private static final String BOOK_TITLE = "new title";
    private static final String BOOK_GENRE = "new genre";
    private static final String BOOK_AUTHOR_1 = "new author 1";
    private static final String BOOK_AUTHOR_2 = "new author 2";

    private static final String NEW_BOOK_TITLE = "Title 2";

    @Autowired
    private BookDaoJdbc bookDaoJdbc;
    @Autowired
    private GenreDaoJdbc genreDaoJdbc;
    @Autowired
    private AuthorDaoJdbc authorDaoJdbc;

    @Test
    public void createBookTest() {
        Book insertedBook = insertTestBook(BOOK_TITLE, BOOK_GENRE, new String[]{BOOK_AUTHOR_1, BOOK_AUTHOR_2});
        Book expectedBook = bookDaoJdbc.getById(insertedBook.getId());
        assertThat(expectedBook).usingRecursiveComparison().isEqualTo(insertedBook);
    }

    @Test
    public void getBookTest() {
        Book expectedBook = getExpectedBook();
        Book requestedBook = bookDaoJdbc.getById(EXISTING_BOOK_ID);
        assertThat(expectedBook).usingRecursiveComparison().isEqualTo(requestedBook);
    }

    @Test
    public void getAllBooksTest() {
        Book expectedBook = getExpectedBook();
        List<Book> books = bookDaoJdbc.getAll();
        assertThat(books).isNotEmpty().hasSizeGreaterThan(MIN_BOOKS_AMOUNT);
    }

    @Test
    public void updateBookTest() {
        Book insertedBook = insertTestBook(BOOK_TITLE, BOOK_GENRE, new String[]{BOOK_AUTHOR_1, BOOK_AUTHOR_2});
        insertedBook.setTitle(NEW_BOOK_TITLE);
        bookDaoJdbc.update(insertedBook);
        Book expectedBook = bookDaoJdbc.getById(insertedBook.getId());
        assertThat(expectedBook.getTitle()).isEqualTo(NEW_BOOK_TITLE);
    }

    @Test
    public void deleteBookTest() {
        assertThatCode(() -> bookDaoJdbc.getById(EXISTING_BOOK_ID))
                .doesNotThrowAnyException();
        bookDaoJdbc.deleteById(EXISTING_BOOK_ID);
        assertThatThrownBy(() -> bookDaoJdbc.getById(EXISTING_BOOK_ID))
                .isInstanceOf(DataAccessException.class);
    }

    private Book insertTestBook(String title, String genreName, String[] authors) {
        Set<Author> authorsSet = new HashSet<>();
        for(String authorName: authors) {
            Author author = new Author();
            author.setName(authorName);
            long authorId = authorDaoJdbc.insert(author);
            author.setId(authorId);
            authorsSet.add(author);
        }
        Genre genre = new Genre();
        genre.setName(genreName);
        long genreId = genreDaoJdbc.insert(genre);
        genre.setId(genreId);

        Book bookToInsert = new Book();
        bookToInsert.setTitle(title);
        bookToInsert.setGenre(genre);
        bookToInsert.setAuthors(authorsSet);
        long bookId = bookDaoJdbc.insert(bookToInsert);
        bookToInsert.setId(bookId);
        return bookToInsert;
    }

    private Book getExpectedBook() {
        Genre genre = new Genre(EXISTING_GENRE_ID, EXISTING_GENRE_NAME);
        Author author1 = new Author(EXISTING_AUTHOR1_ID, EXISTING_AUTHOR1_NAME);
        Author author2 = new Author(EXISTING_AUTHOR2_ID, EXISTING_AUTHOR2_NAME);
        Set<Author> authorSet = new HashSet<>();
        authorSet.add(author1);
        authorSet.add(author2);
        return new Book(EXISTING_BOOK_ID, EXISTING_BOOK_TITLE, authorSet, genre);
    }
}
