insert into genres (id, name) values (100, 'Genre');
insert into authors (id, name) values (100, 'Author 1');
insert into authors (id, name) values (101, 'Author 2');
insert into books (id, title, genre_id) values (100, 'Title', 100);
insert into book_author (book_id, author_id) values (100, 100);
insert into book_author (book_id, author_id) values (100, 101);