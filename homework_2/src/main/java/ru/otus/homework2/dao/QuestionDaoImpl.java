package ru.otus.homework2.dao;

import org.springframework.core.io.Resource;
import ru.otus.homework2.domain.Question;
import ru.otus.homework2.exception.QuestionsSourceException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class QuestionDaoImpl implements QuestionDao {

    private final Resource csv;

    public QuestionDaoImpl(Resource csv) {
        this.csv = csv;
    }

    @Override
    public List<Question> getAllQuestions() throws QuestionsSourceException {
        List<Question> questions = new ArrayList<>();
        try (Scanner scanner = new Scanner(csv.getInputStream())) {
            while (scanner.hasNextLine()) {
                String[] arr = scanner.nextLine().split(";");
                String question = arr[0];
                List<String> answers = Arrays.asList(arr[1].split(":"));
                String rightAnswer = arr[2];
                questions.add(new Question(question, answers, rightAnswer));
            }
        } catch (IOException e) {
            throw new QuestionsSourceException(QuestionsSourceException.NOT_FOUND);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new QuestionsSourceException(QuestionsSourceException.WRONG_FORMAT);
        }
        return questions;
    }
}
