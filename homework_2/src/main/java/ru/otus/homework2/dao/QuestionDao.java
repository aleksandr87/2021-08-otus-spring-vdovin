package ru.otus.homework2.dao;

import ru.otus.homework2.domain.Question;
import ru.otus.homework2.exception.QuestionsSourceException;

import java.util.List;

public interface QuestionDao {
    List<Question> getAllQuestions() throws QuestionsSourceException;
}
