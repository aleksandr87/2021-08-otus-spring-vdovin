package ru.otus.homework2.service;

import ru.otus.homework2.exception.QuestionsSourceException;

public interface InOutService {

    void printMessage(String message);

    String readInput() throws QuestionsSourceException;
}
