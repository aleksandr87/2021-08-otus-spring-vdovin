package ru.otus.homework2.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.otus.homework2.exception.QuestionsSourceException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

@Service
public class ConsoleInOutServiceImpl implements InOutService {

    private final PrintStream printStream;
    private final BufferedReader bufferedReader;

    public ConsoleInOutServiceImpl(@Value("#{ T(java.lang.System).out}") PrintStream printStream,
                                   @Value("#{ T(java.lang.System).in}") InputStream inputStream) {
        this.printStream = printStream;
        this.bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
    }

    @Override
    public void printMessage(String message) {
        printStream.println(message);
    }

    @Override
    public String readInput() throws QuestionsSourceException {
        String input = "";
        try {
            input = bufferedReader.readLine();
        } catch (IOException e) {
            throw new QuestionsSourceException(QuestionsSourceException.READ_INPUT_ERROR);
        }
        return input;
    }
}
