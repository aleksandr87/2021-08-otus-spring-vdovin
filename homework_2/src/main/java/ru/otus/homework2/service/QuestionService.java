package ru.otus.homework2.service;

import ru.otus.homework2.domain.Question;
import ru.otus.homework2.exception.QuestionsSourceException;

import java.util.List;

public interface QuestionService {
    List<Question> getContent() throws QuestionsSourceException;
}
