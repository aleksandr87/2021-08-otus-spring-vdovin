package ru.otus.homework2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.otus.homework2.domain.Question;
import ru.otus.homework2.exception.QuestionsSourceException;

import java.util.List;

@Service
public class TestExecuteServiceImpl implements TestExecuteService {


    private final int maxAllowedWrongAnswers;
    private final InOutService inOutService;
    private final QuestionService questionService;

    @Autowired
    TestExecuteServiceImpl(QuestionService questionService,
                           InOutService printerService,
                           @Value("${questions.maxAllowedWrongAnswers}") int maxAllowedWrongAnswers) {
        this.inOutService = printerService;
        this.questionService = questionService;
        this.maxAllowedWrongAnswers = maxAllowedWrongAnswers;
    }

    @Override
    public void executeTest() {
        String studentName = askStudentName();
        int wrongAnswersCount = askQuestions();
        printResults(studentName, wrongAnswersCount);
    }

    private String askStudentName() {
        String studentName = "";
        inOutService.printMessage("Enter your first and last names please:");
        do {
            try {
                studentName = inOutService.readInput();
                if (studentName == null || studentName.isEmpty()) {
                    inOutService.printMessage("Your name can not be empty!");
                }
            } catch (QuestionsSourceException e) {
                inOutService.printMessage(e.getMessage());
            }
        } while (studentName == null || studentName.isEmpty());
        return studentName;
    }

    private int askQuestions() {
        List<Question> questions;
        int wrongAnswersCount = 0;
        try {
            questions = questionService.getContent();
            for (int i = 0; i < questions.size(); i++) {
                Question question = questions.get(i);
                printQuestion(question);
                String studentsAnswer = inOutService.readInput();
                if (checkAnswerIsWrong(studentsAnswer, question.getRightAnswer())) wrongAnswersCount++;
            }
        } catch (QuestionsSourceException e) {
            inOutService.printMessage(e.getMessage());
            System.exit(1);
        }
        return wrongAnswersCount;
    }

    private void printQuestion(Question question) {
        String questionToPrint = String.format("%s", question.getQuestion());
        inOutService.printMessage(questionToPrint);
        List<String> answers = question.getAnswers();
        if (answers.size() > 0) {
            for (int j = 0; j < answers.size(); j++) {
                String answersToPrint = String.format("%d) %s; ", j + 1, answers.get(j));
                inOutService.printMessage(answersToPrint);
            }
        }
        inOutService.printMessage("Please, choose one answer:");
    }

    private boolean checkAnswerIsWrong(String studentsAnswer, String rightAnswer) {
        return !studentsAnswer.trim().equalsIgnoreCase(rightAnswer);
    }

    private void printResults(String studentName, int wrongAnswersNumber) {
        if (wrongAnswersNumber == 0) {
            inOutService.printMessage(String.format("%s, your result: PASSED. All your answers are correct.", studentName));
        } else if (wrongAnswersNumber <= maxAllowedWrongAnswers) {
            inOutService.printMessage(String.format("%s, your result: PASSED. You have answered %d answers incorrectly.", studentName, wrongAnswersNumber));
        } else {
            inOutService.printMessage(String.format("%s, your result: NOT PASSED. You have answered %d answers incorrectly.", studentName, wrongAnswersNumber));
        }
    }
}
