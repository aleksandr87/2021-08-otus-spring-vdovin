package ru.otus.homework2.exception;

public class QuestionsSourceException extends Exception {

    public final static String NOT_FOUND = "File with questions was not found!";
    public final static String WRONG_FORMAT = "File with questions has wrong format!";
    public final static String READ_INPUT_ERROR = "Something went wrong, please, try again!";

    private String errorMessage;

    public QuestionsSourceException(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getMessage() {
        return errorMessage;
    }
}
