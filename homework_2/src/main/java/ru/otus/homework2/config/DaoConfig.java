package ru.otus.homework2.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import ru.otus.homework2.dao.QuestionDao;
import ru.otus.homework2.dao.QuestionDaoImpl;

@Configuration
public class DaoConfig {

    private final String csv;

    public DaoConfig(@Value("${file.name}") String csv) {
        this.csv = csv;
    }

    @Bean
    public QuestionDao questionDao() {
        Resource resource = new ClassPathResource(csv);
        return new QuestionDaoImpl(resource);
    }
}
