package ru.otus.homework2.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.otus.homework2.dao.QuestionDao;
import ru.otus.homework2.domain.Question;
import ru.otus.homework2.exception.QuestionsSourceException;


import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class QuestionServiceImplTest {

    @Mock
    private QuestionDao questionDao;

    @InjectMocks
    private QuestionServiceImpl service;

    private List<Question> questionList = new ArrayList<>();
    private List<Question> emptyQuestionList = new ArrayList<>();
    private static final String sourceNotFoundMessage = QuestionsSourceException.NOT_FOUND;
    private static final String sourceHasWrongFormat = QuestionsSourceException.WRONG_FORMAT;

    @BeforeEach
    void setUp() {
        List<String> answerList = new ArrayList<>();
        answerList.add("answer 1");
        Question question = new Question("question 1", answerList, "right answer");
        questionList.add(question);
    }

    @Test
    public void testGetContent() throws QuestionsSourceException {
        given(questionDao.getAllQuestions()).willReturn(questionList);
        assertThat(service.getContent()).hasSize(1);
        assertThat(questionList.get(0).getQuestion()).isEqualTo("question 1");
        assertThat(questionList.get(0).getAnswers().get(0)).isEqualTo("answer 1");
        assertThat(questionList.get(0).getRightAnswer()).isEqualTo("right answer");
    }

    @Test
    public void testGetEmptyContent() throws QuestionsSourceException {
        given(questionDao.getAllQuestions()).willReturn(emptyQuestionList);
        assertThat(service.getContent()).hasSize(0);
    }

    @Test
    public void testSourceNotFound() throws QuestionsSourceException {
        given(questionDao.getAllQuestions()).willThrow(new QuestionsSourceException(QuestionsSourceException.NOT_FOUND));
        Exception exception = assertThrows(QuestionsSourceException.class, () -> service.getContent());
        assertThat(exception.getMessage()).isEqualTo(sourceNotFoundMessage);
    }

    @Test
    public void testSourceHasWrongFormat() throws QuestionsSourceException {
        given(questionDao.getAllQuestions()).willThrow(new QuestionsSourceException(QuestionsSourceException.WRONG_FORMAT));
        Exception exception = assertThrows(QuestionsSourceException.class, () -> service.getContent());
        assertThat(exception.getMessage()).isEqualTo(sourceHasWrongFormat);
    }
}
