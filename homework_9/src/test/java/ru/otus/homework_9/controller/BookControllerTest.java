package ru.otus.homework_9.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ru.otus.homework_9.entity.Author;
import ru.otus.homework_9.entity.Book;
import ru.otus.homework_9.entity.CommentToBook;
import ru.otus.homework_9.entity.Genre;
import ru.otus.homework_9.exception.RepositoryException;
import ru.otus.homework_9.form.BookForm;
import ru.otus.homework_9.service.BookService;
import ru.otus.homework_9.service.CommentToBookService;

import java.util.List;
import java.util.Set;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(BookController.class)
public class BookControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BookService bookService;

    @MockBean
    private CommentToBookService commentToBookService;

    @Test
    public void showBookTest() throws Exception {
        Book book = getBook();
        List<CommentToBook> comments = List.of(new CommentToBook(1L, "Test comment", book));

        given(bookService.getBook(anyLong())).willReturn(book);
        given(commentToBookService.getAllCommentsToBook(anyLong())).willReturn(comments);

        mockMvc.perform(get("/books/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("book"))
                .andExpect(model().attributeExists("bookForm", "comments", "commentForm"));
    }

    @Test
    public void bookNotFoundTest() throws Exception {
        given(bookService.getBook(anyLong())).willThrow(new RepositoryException("not found"));
        mockMvc.perform(get("/books/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("error"))
                .andExpect(model().attributeExists("errorMessage"));
    }

    @Test
    public void initEditingFormTest() throws Exception {
        Book book = getBook();

        given(bookService.getBook(anyLong())).willReturn(book);

        mockMvc.perform(get("/books/1/edit"))
                .andExpect(status().isOk())
                .andExpect(view().name("createOrEditBook"))
                .andExpect(model().attributeExists("bookForm"));
    }

    @Test
    public void initEditingFormWithFailureTest() throws Exception {
        given(bookService.getBook(anyLong())).willThrow(new RepositoryException("not found"));

        mockMvc.perform(get("/books/1/edit"))
                .andExpect(status().isOk())
                .andExpect(view().name("error"))
                .andExpect(model().attributeExists("errorMessage"));
    }

    @Test
    public void submitEditingFormTest() throws Exception {
        Book book = getBook();

        given(bookService.updateBook(1L, "Test book", "Test genre", List.of("Test author"))).willReturn(book);

        mockMvc.perform(post("/books/1/edit")
                        .flashAttr("bookForm", new BookForm(1L, "test book", "test genre", "test author")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/books/1"));
    }

    @Test void deleteBookTest() throws Exception {
        mockMvc.perform(post("/books/1/delete"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/books"));
        verify(commentToBookService, times(1)).deleteAllCommentsFromBook(anyLong());
        verify(bookService, times(1)).deleteBook(anyLong());
    }

    private Book getBook() {
        Genre genre = new Genre(1L,"Test genre");
        Author author = new Author(1L, "Test author");
        return new Book(1L, "Test book", Set.of(author), genre);
    }
}
