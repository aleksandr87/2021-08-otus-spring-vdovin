package ru.otus.homework_9.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ru.otus.homework_9.form.CommentForm;
import ru.otus.homework_9.service.CommentToBookService;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(CommentToBookController.class)
public class CommentToBookControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CommentToBookService commentToBookService;

    @Test
    public void addComment() throws Exception {

        mockMvc.perform(post("/comments/1/new")
                        .flashAttr("commentForm", new CommentForm("test comment")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/books/1"));
        verify(commentToBookService, times(1)).addCommentToBook(anyLong(), anyString());
    }
}
