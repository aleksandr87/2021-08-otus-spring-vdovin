package ru.otus.homework_9.exception;

public class RepositoryException extends Exception{

    public static final String AUTHOR_NAME_NOT_EXISTS = "Author with name %s doesn't exist";
    public static final String AUTHOR_ID_NOT_EXISTS = "Author with ID %s doesn't exist";
    public static final String AUTHOR_ALREADY_EXISTS = "Author with name %s already exists";
    public static final String GENRE_NAME_NOT_EXISTS = "Genre with name %s doesn't exist";
    public static final String GENRE_ID_NOT_EXISTS = "Genre with ID %s doesn't exist";
    public static final String GENRE_ALREADY_EXISTS = "Genre with name %s already exists";
    public static final String BOOK_ID_NOT_EXISTS = "Book with ID %s doesn't exist";
    public static final String BOOK_TITLE_NOT_EXISTS = "Book with title %s doesn't exist";
    public static final String COMMENT_ID_NOT_EXISTS = "Comment with ID %s doesn't exist";

    private final String errorMessage;
    private final String queryParam;

    public RepositoryException(String errorMessage) {
        this.errorMessage = errorMessage;
        this.queryParam = "";
    }

    public RepositoryException(String errorMessage, String queryParam) {
        this.errorMessage = errorMessage;
        this.queryParam = queryParam;
    }

    public RepositoryException(String errorMessage, String queryParam, Throwable cause) {
        super(cause);
        this.errorMessage = errorMessage;
        this.queryParam = queryParam;
    }

    public String getMessage() {
        return String.format(errorMessage, queryParam) + (getCause() != null ? ": "+getCause() : "");
    }
}
