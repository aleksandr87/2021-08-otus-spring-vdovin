package ru.otus.homework_9.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.otus.homework_9.entity.Genre;

import java.util.Optional;

public interface GenreRepository extends JpaRepository<Genre, Long> {

    Optional<Genre> findByName(String s);
}
