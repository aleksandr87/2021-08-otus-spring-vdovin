package ru.otus.homework_9.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.otus.homework_9.entity.CommentToBook;

import java.util.List;

public interface CommentToBookRepository extends JpaRepository<CommentToBook, Long> {

    List<CommentToBook> getAllByBookId(Long id);

    void deleteAllByBookId(Long id);
}
