package ru.otus.homework_9.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.otus.homework_9.exception.RepositoryException;
import ru.otus.homework_9.form.CommentForm;
import ru.otus.homework_9.service.CommentToBookService;

@RequestMapping("/comments/{bookId}")
@Controller
public class CommentToBookController {

    private final CommentToBookService commentToBookService;

    @Autowired
    public CommentToBookController(CommentToBookService commentToBookService) {
        this.commentToBookService = commentToBookService;
    }

    @PostMapping("/new")
    public String addComment(Model model, @ModelAttribute("commentForm") CommentForm commentForm, @PathVariable Long bookId) throws RepositoryException {
        if(commentForm.getNewComment() != null && !commentForm.getNewComment().isEmpty()) {
            commentToBookService.addCommentToBook(bookId, commentForm.getNewComment());
        }
        return "redirect:/books/" + bookId;
    }
}
