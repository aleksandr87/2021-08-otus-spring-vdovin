package ru.otus.homework_9.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.otus.homework_9.entity.Author;
import ru.otus.homework_9.entity.Book;
import ru.otus.homework_9.entity.CommentToBook;
import ru.otus.homework_9.exception.RepositoryException;
import ru.otus.homework_9.form.BookForm;
import ru.otus.homework_9.form.CommentForm;
import ru.otus.homework_9.service.BookService;
import ru.otus.homework_9.service.CommentToBookService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class BookController {

    private static final String ERROR_MESSAGE = "Please, enter at least title!";

    private final BookService bookService;
    private final CommentToBookService commentToBookService;

    @Autowired
    public BookController(BookService bookService, CommentToBookService commentToBookService) {
        this.bookService = bookService;
        this.commentToBookService = commentToBookService;
    }

    @GetMapping({"/", "/books", "/index.html"})
    public String listBooks(Model model) {
        List<Book> books = bookService.getAllBooks();
        model.addAttribute("books", books);
        return "index";
    }

    @GetMapping("/books/{id}")
    public String showBook(@PathVariable Long id ,Model model) throws RepositoryException {
        Book book = bookService.getBook(id);
        List<CommentToBook> comments = commentToBookService.getAllCommentsToBook(id);
        BookForm bookForm = new BookForm(book.getId(), book.getTitle(), book.getGenre().getName(),
                book.getAuthors().stream().map(Author::getName).collect(Collectors.joining(" ; ")));
        model.addAttribute("bookForm", bookForm);
        model.addAttribute("comments", comments);
        model.addAttribute("commentForm", new CommentForm());
        return "book";
    }

    @GetMapping("/books/new")
    public String initCreationForm(Model model){
        model.addAttribute("create", true);
        model.addAttribute("bookForm", new BookForm());
        return "createOrEditBook";
    }

    @PostMapping("/books/new")
    public String submitCreationForm(Model model, @ModelAttribute("bookForm") BookForm bookForm, BindingResult result) {
        if (bookForm.getTitle() == null || bookForm.getTitle().isEmpty()) {
            model.addAttribute("errorMessage", ERROR_MESSAGE);
            return "createOrEditBook";
        }
        List<String> authors = Arrays.stream(bookForm.getAuthors().split("[,;]+"))
                .map(String::trim)
                .collect(Collectors.toList());
        Book book = bookService.createBook(bookForm.getTitle(), bookForm.getGenre(), authors);
        return "redirect:/books/" + book.getId();
    }

    @GetMapping("/books/{id}/edit")
    public String initEditingForm(Model model, @PathVariable Long id){
        Book book = null;
        try {
            book = bookService.getBook(id);
        } catch (RepositoryException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "error";
        }
        BookForm bookForm = new BookForm(book.getId(), book.getTitle(), book.getGenre().getName(),
                book.getAuthors().stream().map(Author::getName).collect(Collectors.joining(" ; ")));
        model.addAttribute("bookForm", bookForm);
        model.addAttribute("create", false);
        return "createOrEditBook";
    }

    @PostMapping("/books/{id}/edit")
    public String submitEditingForm(Model model, @Valid @ModelAttribute("bookForm") BookForm bookForm, @PathVariable Long id) throws RepositoryException {
        if(bookForm.getTitle() == null || bookForm.getTitle().isEmpty()) {
            model.addAttribute("errorMessage", ERROR_MESSAGE);
            model.addAttribute("create", false);
            return "createOrEditBook";
        }
        List<String> authors = Arrays.stream(bookForm.getAuthors().split("[,;]+"))
                .map(String::trim)
                .collect(Collectors.toList());
        bookService.updateBook(id, bookForm.getTitle(), bookForm.getGenre(), authors);
        return "redirect:/books/" + id;
    }

    @PostMapping("/books/{id}/delete")
    public String deleteBook(Model model, @PathVariable Long id) throws RepositoryException {
        commentToBookService.deleteAllCommentsFromBook(id);
        bookService.deleteBook(id);
        return "redirect:/books";
    }
}
