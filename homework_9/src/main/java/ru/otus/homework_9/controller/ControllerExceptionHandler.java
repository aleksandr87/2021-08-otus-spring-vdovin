package ru.otus.homework_9.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import ru.otus.homework_9.exception.RepositoryException;

@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(RepositoryException.class)
    public ModelAndView handleException(RepositoryException exception){

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("error");
        modelAndView.addObject("errorMessage", exception.getMessage());
        return modelAndView;
    }
}
