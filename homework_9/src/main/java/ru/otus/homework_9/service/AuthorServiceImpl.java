package ru.otus.homework_9.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.otus.homework_9.entity.Author;
import ru.otus.homework_9.exception.RepositoryException;
import ru.otus.homework_9.repository.AuthorRepository;

@Service
public class AuthorServiceImpl implements AuthorService{

    private final AuthorRepository authorRepository;

    @Autowired
    public AuthorServiceImpl(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    @Transactional
    public Author createAuthor(String authorName){
        Author author = new Author();
        author.setName(authorName);
        return authorRepository.save(author);
    }

    @Override
    @Transactional(readOnly = true)
    public Author getAuthor(long id) throws RepositoryException {
        return authorRepository.findById(id).orElseThrow(
                () -> new RepositoryException(RepositoryException.AUTHOR_ID_NOT_EXISTS, String.valueOf(id))
        );
    }

    @Override
    @Transactional(readOnly = true)
    public Author getByName(String name) throws RepositoryException {
        return authorRepository.findByName(name).orElseThrow(
                () -> new RepositoryException(RepositoryException.AUTHOR_NAME_NOT_EXISTS, name)
        );
    }

    @Override
    @Transactional
    public Author updateAuthor(long id, String name) throws RepositoryException {
        Author author = getAuthor(id);
        author.setName(name);
        return authorRepository.save(author);
    }

    @Override
    @Transactional
    public void deleteAuthor(long id) throws RepositoryException {
        Author author = getAuthor(id);
        authorRepository.delete(author);
    }
}
