package ru.otus.homework_9.service;

import ru.otus.homework_9.entity.Book;
import ru.otus.homework_9.exception.RepositoryException;

import java.util.List;

public interface BookService {

    Book createBook(String title, String genre, List<String> authors);

    Book getBook(long id) throws RepositoryException;

    List<Book> getAllBooks();

    Book updateBook(long id, String title, String genre, List<String> authors) throws RepositoryException;

    void deleteBook(long id) throws RepositoryException;
}
