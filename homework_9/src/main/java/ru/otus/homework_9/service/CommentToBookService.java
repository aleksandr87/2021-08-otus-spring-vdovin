package ru.otus.homework_9.service;

import ru.otus.homework_9.entity.CommentToBook;
import ru.otus.homework_9.exception.RepositoryException;

import java.util.List;

public interface CommentToBookService {

    List<CommentToBook> getAllCommentsToBook(long bookId) throws RepositoryException;

    void addCommentToBook(long bookId, String comment) throws RepositoryException;

    void deleteCommentFromBook(long id) throws RepositoryException;

    void deleteAllCommentsFromBook(long id) throws RepositoryException;
}
