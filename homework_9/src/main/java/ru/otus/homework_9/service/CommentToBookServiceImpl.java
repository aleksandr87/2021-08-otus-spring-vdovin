package ru.otus.homework_9.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.otus.homework_9.entity.Book;
import ru.otus.homework_9.entity.CommentToBook;
import ru.otus.homework_9.exception.RepositoryException;
import ru.otus.homework_9.repository.CommentToBookRepository;

import java.util.List;

@Service
public class CommentToBookServiceImpl implements CommentToBookService{

    private final CommentToBookRepository commentToBookRepository;
    private final BookService bookService;

    @Autowired
    CommentToBookServiceImpl(CommentToBookRepository commentToBookRepository, BookService bookService) {
        this.commentToBookRepository = commentToBookRepository;
        this.bookService = bookService;
    }

    @Override
    @Transactional(readOnly = true)
    public List<CommentToBook> getAllCommentsToBook(long bookId) throws RepositoryException {
        Book book = bookService.getBook(bookId);
        return commentToBookRepository.getAllByBookId(book.getId());
    }

    @Override
    @Transactional
    public void addCommentToBook(long bookId, String comment) throws RepositoryException {
        Book book = bookService.getBook(bookId);
        CommentToBook commentToBook = new CommentToBook();
        commentToBook.setComment(comment);
        commentToBook.setBook(book);
        commentToBookRepository.save(commentToBook);
    }

    @Override
    @Transactional
    public void deleteCommentFromBook(long id) throws RepositoryException {
        CommentToBook commentToBook = getCommentById(id);
        commentToBookRepository.delete(commentToBook);
    }

    @Override
    @Transactional
    public void deleteAllCommentsFromBook(long bookId) throws RepositoryException {
        Book book = bookService.getBook(bookId);
        commentToBookRepository.deleteAllByBookId(book.getId());
    }

    private CommentToBook getCommentById(long id) throws RepositoryException {
        return commentToBookRepository.findById(id).orElseThrow(
                () -> new RepositoryException(RepositoryException.COMMENT_ID_NOT_EXISTS, String.valueOf(id))
        );
    }
}
