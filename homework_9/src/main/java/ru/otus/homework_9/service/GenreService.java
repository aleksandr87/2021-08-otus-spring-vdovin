package ru.otus.homework_9.service;

import ru.otus.homework_9.entity.Genre;
import ru.otus.homework_9.exception.RepositoryException;

public interface GenreService {
    Genre createGenre(String genreName);

    Genre getGenre(long id) throws RepositoryException;

    Genre getByName(String name) throws RepositoryException;

    Genre updateGenre(long id, String name) throws RepositoryException;

    void deleteGenre(long id) throws RepositoryException;
}
