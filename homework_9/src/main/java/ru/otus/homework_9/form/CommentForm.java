package ru.otus.homework_9.form;

public class CommentForm {

    private String newComment;

    public CommentForm() {
    }

    public CommentForm(String newComment) {
        this.newComment = newComment;
    }

    public String getNewComment() {
        return newComment;
    }

    public void setNewComment(String newComment) {
        this.newComment = newComment;
    }
}
