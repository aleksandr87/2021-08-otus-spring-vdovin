package ru.otus.homework_9.form;


import javax.validation.constraints.NotBlank;

public class BookForm {

    private Long id;
    private String title;
    private String genre;
    private String authors;

    public BookForm() {
    }

    public BookForm(Long id, String title, String genre, String authors) {
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.authors = authors;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }
}
