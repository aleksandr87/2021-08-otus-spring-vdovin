package ru.otus.homework1.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.otus.homework1.dao.QuestionDao;
import ru.otus.homework1.domain.Question;


import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class QuestionServiceImplTest {

    @Mock
    private QuestionDao questionDao;

    @InjectMocks
    private QuestionServiceImpl service;

    private List<Question> questionList = new ArrayList<>();
    private List<Question> emptyQuestionList = new ArrayList<>();

    @BeforeEach
    void setUp() {
        List<String> answerList = new ArrayList<>();
        answerList.add("answer 1");
        Question question = new Question("question 1", answerList);
        questionList.add(question);
    }

    @Test
    public void testGetContent() {
        given(questionDao.getAllQuestions()).willReturn(questionList);
        assertThat(service.getContent()).hasSize(1);
        assertThat(questionList.get(0).getQuestion()).isEqualTo("question 1");
        assertThat(questionList.get(0).getAnswers().get(0)).isEqualTo("answer 1");
    }

    @Test
    public void testGetEmptyContent() {
        given(questionDao.getAllQuestions()).willReturn(emptyQuestionList);
        assertThat(service.getContent()).hasSize(0);
    }
}
