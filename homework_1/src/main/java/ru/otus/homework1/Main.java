package ru.otus.homework1;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.otus.homework1.service.PrinterService;
import ru.otus.homework1.service.PrinterServiceImpl;

public class Main {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/spring-context.xml");
        PrinterService service = context.getBean(PrinterService.class);
        service.printQuestions();
    }
}
