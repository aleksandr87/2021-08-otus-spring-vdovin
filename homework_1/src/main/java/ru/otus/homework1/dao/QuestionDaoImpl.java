package ru.otus.homework1.dao;

import org.springframework.core.io.Resource;
import ru.otus.homework1.domain.Question;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class QuestionDaoImpl implements QuestionDao {

    private final Resource csv;

    public QuestionDaoImpl(Resource csv) {
        this.csv = csv;
    }

    @Override
    public List<Question> getAllQuestions() {
        List<Question> questions = new ArrayList<>();
        try (Scanner scanner = new Scanner(csv.getInputStream())) {
            while (scanner.hasNextLine()) {
                String[] arr = scanner.nextLine().split(";");
                String question = arr[0];
                List<String> answers = new ArrayList<>();
                for (int i = 1; i < arr.length; i++) {
                    answers.add(arr[i]);
                }
                questions.add(new Question(question, answers));
            }
        } catch (IOException e) {
            System.out.println("File with questions was not found.");
        }
        return questions;
    }
}
