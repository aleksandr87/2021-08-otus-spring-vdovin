package ru.otus.homework1.service;

import ru.otus.homework1.domain.Question;

import java.io.PrintStream;
import java.util.List;

public class ConsolePrintServiceImpl implements ConsolePrintService{

    private final PrintStream printStream;

    public ConsolePrintServiceImpl(PrintStream printStream) {
        this.printStream = printStream;
    }

    @Override
    public void printToConsole(List<Question> questions) {
        for (int i = 0; i < questions.size(); i++) {
            printStream.printf("%d. %s ", i + 1, questions.get(i).getQuestion());
            List<String> answers = questions.get(i).getAnswers();
            if (answers.size() > 0) {
                for (int j = 0; j < answers.size(); j++) {
                    printStream.printf("%d) %s; ", j + 1, answers.get(j));
                }
            }
            printStream.printf("%n");
        }
    }
}
