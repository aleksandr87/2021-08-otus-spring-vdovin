package ru.otus.homework1.service;

import ru.otus.homework1.domain.Question;

import java.util.List;

public interface ConsolePrintService {
    public void printToConsole(List<Question> questions);
}
