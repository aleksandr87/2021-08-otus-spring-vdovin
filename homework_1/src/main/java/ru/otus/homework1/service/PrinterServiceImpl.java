package ru.otus.homework1.service;

import ru.otus.homework1.domain.Question;

import java.util.List;

public class PrinterServiceImpl implements PrinterService {

    private final QuestionService questionService;
    private final ConsolePrintService consolePrintService;

    public PrinterServiceImpl(QuestionService questionService) {
        this.questionService = questionService;
        this.consolePrintService = new ConsolePrintServiceImpl(System.out);
    }

    @Override
    public void printQuestions() {
        List<Question> questions = questionService.getContent();
        consolePrintService.printToConsole(questions);
    }
}
