package ru.otus.homework_6.repository;

import org.springframework.stereotype.Repository;
import ru.otus.homework_6.entity.CommentToBook;
import ru.otus.homework_6.exception.RepositoryException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@Repository
public class CommentToBookRepositoryImpl implements CommentToBookRepository {

    @PersistenceContext
    private final EntityManager em;

    CommentToBookRepositoryImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public Optional<CommentToBook> getById(long id) {
        CommentToBook commentToBook = em.find(CommentToBook.class, id);
        return Optional.ofNullable(commentToBook);
    }

    @Override
    public List<CommentToBook> getAllByBook(long bookId) {
        TypedQuery<CommentToBook> query = em.createQuery("select c from CommentToBook c inner join Book b on c.book = b where b.id =: bookId", CommentToBook.class);
        query.setParameter("bookId", bookId);
        return query.getResultList();
    }

    @Override
    public CommentToBook insert(CommentToBook commentToBook){
        em.persist(commentToBook);
        return commentToBook;
    }

    @Override
    public void deleteFromBook(CommentToBook commentToBook) {
        em.remove(commentToBook);
    }
}
