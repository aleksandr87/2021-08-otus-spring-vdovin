package ru.otus.homework_6.repository;

import ru.otus.homework_6.entity.CommentToBook;
import ru.otus.homework_6.exception.RepositoryException;

import java.util.List;
import java.util.Optional;

public interface CommentToBookRepository {

    Optional<CommentToBook> getById(long id);

    List<CommentToBook> getAllByBook(long bookId) throws RepositoryException;

    CommentToBook insert(CommentToBook commentToBook);

    void deleteFromBook(CommentToBook commentToBook) throws RepositoryException;
}
