package ru.otus.homework_6.repository;

import ru.otus.homework_6.entity.Author;

import javax.persistence.NoResultException;
import java.util.Optional;

public interface AuthorRepository {

    Author insert(Author author);

    Optional<Author> getById(long id);

    Author getByName(String name) throws NoResultException;

    Author update(Author author);

    void delete(Author author);
}
