package ru.otus.homework_6.repository;

import ru.otus.homework_6.entity.Genre;

import javax.persistence.NoResultException;
import java.util.Optional;

public interface GenreRepository {

    Genre insert(Genre genre);

    Optional<Genre> getById(long id);

    Optional<Genre> getByName(String name) throws NoResultException;

    Genre update(Genre genre);

    void delete(Genre genre);
}
