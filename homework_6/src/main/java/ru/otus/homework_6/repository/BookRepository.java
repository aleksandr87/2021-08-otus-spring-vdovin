package ru.otus.homework_6.repository;

import ru.otus.homework_6.entity.Book;

import javax.persistence.NoResultException;
import java.util.List;

public interface BookRepository {

    Book insert(Book book);

    Book getById(long id) throws NoResultException;

    List<Book> getAll();

    Book update(Book book);

    void deleteById(Book book);

    Book getByIdWithComments(long id) throws NoResultException;
}
