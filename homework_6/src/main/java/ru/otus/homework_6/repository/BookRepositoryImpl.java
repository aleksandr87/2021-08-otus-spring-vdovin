package ru.otus.homework_6.repository;

import org.springframework.stereotype.Repository;
import ru.otus.homework_6.entity.Book;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class BookRepositoryImpl implements BookRepository{

    @PersistenceContext
    private final EntityManager em;

    public BookRepositoryImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public Book insert(Book book) {
        em.persist(book);
        return book;
    }

    @Override
    public Book getById(long id) throws NoResultException {
        TypedQuery<Book> query = em.createQuery("select b from Book b where b.id = :id", Book.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }

    @Override
    public List<Book> getAll() {
        return em.createQuery("select b from Book b join fetch b.genre g", Book.class).getResultList();
    }

    @Override
    public Book update(Book book) {
        return em.merge(book);
    }

    @Override
    public void deleteById(Book book) {
        em.remove(book);
    }

    @Override
    public Book getByIdWithComments(long id) throws NoResultException {
        TypedQuery<Book> query = em.createQuery("select b from Book b left join fetch b.comments c where b.id = :id", Book.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }
}
