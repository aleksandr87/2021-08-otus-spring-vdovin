package ru.otus.homework_6.repository;

import ru.otus.homework_6.entity.Author;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Optional;

@Repository
public class AuthorRepositoryImpl implements AuthorRepository{

    @PersistenceContext
    private final EntityManager em;

    public AuthorRepositoryImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public Author insert(Author author) {
        em.persist(author);
        return author;
    }

    @Override
    public Optional<Author> getById(long id) {
        Author author = em.find(Author.class, id);
        return Optional.ofNullable(author);
    }

    @Override
    public Author getByName(String name) throws NoResultException {
        TypedQuery<Author> query = em.createQuery("select a from Author a where a.name = :name", Author.class);
        query.setParameter("name", name);
        return query.getSingleResult();
    }

    @Override
    public Author update(Author author) {
        return em.merge(author);
    }

    @Override
    public void delete(Author author) {
        em.remove(author);
    }
}
