package ru.otus.homework_6.service;

import ru.otus.homework_6.entity.Author;
import ru.otus.homework_6.exception.RepositoryException;

public interface AuthorService {

    Author createAuthor(String authorName);

    Author getAuthor(long id) throws RepositoryException;

    Author getByName(String name) throws RepositoryException;

    Author updateAuthor(long id, String name) throws RepositoryException;

    void deleteAuthor(long id) throws RepositoryException;
}
