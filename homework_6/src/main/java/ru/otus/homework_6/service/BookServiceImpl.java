package ru.otus.homework_6.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.otus.homework_6.entity.Author;
import ru.otus.homework_6.entity.Book;
import ru.otus.homework_6.entity.Genre;
import ru.otus.homework_6.exception.RepositoryException;
import ru.otus.homework_6.repository.BookRepository;
import javax.persistence.NoResultException;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class BookServiceImpl implements BookService{

    private final BookRepository bookRepository;
    private final GenreService genreService;
    private final AuthorService authorService;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository, GenreService genreService, AuthorService authorService) {
        this.bookRepository = bookRepository;
        this.genreService = genreService;
        this.authorService = authorService;
    }

    @Override
    @Transactional
    public Book createBook(String title, String genreName, String[] authors) {
        Book bookEntity = getEntityToInsert(title, genreName, authors);
        return bookRepository.insert(bookEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Book getBook(long id) throws RepositoryException {
        try {
            Book bookEntity = bookRepository.getById(id);
            bookEntity.getAuthors().size(); // <- для инициализации authors
            return bookEntity;
        }catch (NoResultException e) {
            throw new RepositoryException(RepositoryException.BOOK_ID_NOT_EXISTS, String.valueOf(id), e);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Book> getAllBooks() {
        List<Book> bookEntities = bookRepository.getAll();
        bookEntities.forEach(bookEntity -> bookEntity.getAuthors().size()); // <- для инициализации authors
        return bookEntities;
    }

    @Override
    @Transactional
    public Book updateBook(long id, String title, String genreName, String[] authors) throws RepositoryException {
        Book bookEntity = getEntityToUpdate(id, title, genreName, authors);
        return bookRepository.update(bookEntity);
    }

    @Override
    @Transactional
    public void deleteBook(long id) throws RepositoryException {
        Book bookEntity = getBook(id);
        bookRepository.deleteById(bookEntity);
    }

    private Book getEntityToInsert(String title, String genreName, String[] authors) {
        Book bookEntity = new Book();
        bookEntity.setTitle(title);
        Genre genre = null;
        try {
            genre = genreService.getByName(genreName);
        } catch (RepositoryException e) {
            genre = genreService.createGenre(genreName);
        }
        bookEntity.setGenre(genre);
        Set<Author> authorsSet = new HashSet<>();
        for(String authorName: authors) {
            Author author = null;
            try {
                author = authorService.getByName(authorName);
            } catch (RepositoryException e) {
                author = authorService.createAuthor(authorName);
            }
            author.setName(authorName);
            authorsSet.add(author);
        }
        bookEntity.setAuthors(authorsSet);
        return bookEntity;
    }

    private Book getEntityToUpdate(long id, String title, String genreName, String[] authors) throws RepositoryException {
        Book bookEntity = getBook(id);
        if (title != null) bookEntity.setTitle(title);
        if (genreName != null) {
            Genre genre = null;
            try {
                genre = genreService.getByName(genreName);
            } catch (RepositoryException e) {
                genre = genreService.createGenre(genreName);
            }
            bookEntity.setGenre(genre);
        }
        Set<Author> authorsSet = new HashSet<>();
        for(String authorName: authors) {
            Author author = null;
            try {
                author = authorService.getByName(authorName);
            } catch (RepositoryException e) {
                author = authorService.createAuthor(authorName);
            }
            author.setName(authorName);
            authorsSet.add(author);
        }
        bookEntity.setAuthors(authorsSet);
        return bookEntity;
    }
}
