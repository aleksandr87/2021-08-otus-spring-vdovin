package ru.otus.homework_6.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.otus.homework_6.entity.Author;
import ru.otus.homework_6.exception.RepositoryException;
import ru.otus.homework_6.repository.AuthorRepository;

import javax.persistence.NoResultException;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AuthorServiceImpl implements AuthorService{

    private final AuthorRepository authorRepository;

    @Autowired
    public AuthorServiceImpl(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    @Transactional
    public Author createAuthor(String authorName){
        Author author = new Author();
        author.setName(authorName);
        return authorRepository.insert(author);
    }

    @Override
    @Transactional(readOnly = true)
    public Author getAuthor(long id) throws RepositoryException {
        return authorRepository.getById(id).orElseThrow(
                () -> new RepositoryException(RepositoryException.AUTHOR_ID_NOT_EXISTS, String.valueOf(id))
        );
    }

    @Override
    @Transactional(readOnly = true)
    public Author getByName(String name) throws RepositoryException {
        try {
            return authorRepository.getByName(name);
        } catch (NoResultException e) {
            throw new RepositoryException(RepositoryException.AUTHOR_NAME_NOT_EXISTS, name, e);
        }
    }

    @Override
    @Transactional
    public Author updateAuthor(long id, String name) throws RepositoryException {
        Author author = getAuthor(id);
        author.setName(name);
        return authorRepository.update(author);
    }

    @Override
    @Transactional
    public void deleteAuthor(long id) throws RepositoryException {
        Author author = getAuthor(id);
        authorRepository.delete(author);
    }
}
