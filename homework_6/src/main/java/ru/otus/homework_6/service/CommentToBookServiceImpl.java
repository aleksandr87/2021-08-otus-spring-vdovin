package ru.otus.homework_6.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.otus.homework_6.entity.Book;
import ru.otus.homework_6.entity.CommentToBook;
import ru.otus.homework_6.exception.RepositoryException;
import ru.otus.homework_6.repository.CommentToBookRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CommentToBookServiceImpl implements CommentToBookService{

    private final CommentToBookRepository commentToBookRepository;
    private final BookService bookService;

    @Autowired
    CommentToBookServiceImpl(CommentToBookRepository commentToBookRepository, BookService bookService) {
        this.commentToBookRepository = commentToBookRepository;
        this.bookService = bookService;
    }

    @Override
    @Transactional(readOnly = true)
    public List<CommentToBook> getAllCommentsToBook(long bookId) throws RepositoryException {
        bookService.getBook(bookId);
        return commentToBookRepository.getAllByBook(bookId);
    }

    @Override
    @Transactional
    public void addCommentToBook(long bookId, String comment) throws RepositoryException {
        Book book = bookService.getBook(bookId);
        CommentToBook commentToBook = new CommentToBook();
        commentToBook.setComment(comment);
        commentToBook.setBook(book);
        commentToBookRepository.insert(commentToBook);
    }

    @Override
    @Transactional
    public void deleteCommentFromBook(long id) throws RepositoryException {
        CommentToBook commentToBook = getCommentById(id);
        commentToBookRepository.deleteFromBook(commentToBook);
    }

    private CommentToBook getCommentById(long id) throws RepositoryException {
        return commentToBookRepository.getById(id).orElseThrow(
                () -> new RepositoryException(RepositoryException.COMMENT_ID_NOT_EXISTS, String.valueOf(id))
        );
    }
}
