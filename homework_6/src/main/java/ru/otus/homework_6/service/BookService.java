package ru.otus.homework_6.service;

import ru.otus.homework_6.entity.Book;
import ru.otus.homework_6.exception.RepositoryException;
import java.util.List;

public interface BookService {

    Book createBook(String title, String genre, String[] authors);

    Book getBook(long id) throws RepositoryException;

    List<Book> getAllBooks();

    Book updateBook(long id, String title, String genre, String[] authors) throws RepositoryException;

    void deleteBook(long id) throws RepositoryException;
}
