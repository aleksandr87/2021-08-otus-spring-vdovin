package ru.otus.homework_6.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.otus.homework_6.entity.Genre;
import ru.otus.homework_6.exception.RepositoryException;
import ru.otus.homework_6.repository.GenreRepository;

import org.springframework.transaction.annotation.Transactional;

@Service
public class GenreServiceImpl implements GenreService{

    private final GenreRepository genreRepository;

    @Autowired
    public GenreServiceImpl(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    @Override
    @Transactional
    public Genre createGenre(String genreName) {
        Genre genre = new Genre();
        genre.setName(genreName);
        return genreRepository.insert(genre);
    }

    @Override
    @Transactional(readOnly = true)
    public Genre getGenre(long id) throws RepositoryException {
        return genreRepository.getById(id).orElseThrow(
            () -> new RepositoryException(RepositoryException.GENRE_ID_NOT_EXISTS, String.valueOf(id))
        );
    }

    @Override
    @Transactional(readOnly = true)
    public Genre getByName(String name) throws RepositoryException {
        return genreRepository.getByName(name).orElseThrow(
            () -> new RepositoryException(RepositoryException.GENRE_NAME_NOT_EXISTS, name)
        );
    }

    @Override
    @Transactional
    public Genre updateGenre(long id, String name) throws RepositoryException {
        Genre genre = getGenre(id);
        genre.setName(name);
        return genreRepository.update(genre);
    }

    @Override
    @Transactional
    public void deleteGenre(long id) throws RepositoryException {
        Genre genre = getGenre(id);
        genreRepository.delete(genre);
    }
}
