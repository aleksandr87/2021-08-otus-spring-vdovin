package ru.otus.homework_6.shell;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ru.otus.homework_6.entity.Author;
import ru.otus.homework_6.entity.Book;
import ru.otus.homework_6.entity.CommentToBook;
import ru.otus.homework_6.entity.Genre;
import ru.otus.homework_6.exception.RepositoryException;
import ru.otus.homework_6.service.AuthorService;
import ru.otus.homework_6.service.BookService;
import ru.otus.homework_6.service.CommentToBookService;
import ru.otus.homework_6.service.GenreService;

import java.util.List;
import java.util.stream.Collectors;

@ShellComponent
public class ShellExecutor {

    private final AuthorService authorService;
    private final BookService bookService;
    private final GenreService genreService;
    private final CommentToBookService commentToBookService;

    public ShellExecutor(AuthorService authorService, BookService bookService, GenreService genreService, CommentToBookService commentToBookService) {
        this.authorService = authorService;
        this.bookService = bookService;
        this.genreService = genreService;
        this.commentToBookService = commentToBookService;
    }

    @ShellMethod(value = "get book", key = {"gb", "get book"})
    public String getBook(@ShellOption("--id") long id) {
        try {
            return "Requested book: " + bookService.getBook(id);
        } catch (RepositoryException e) {
            return e.getMessage();
        }
    }

    @ShellMethod(value = "get all books", key = {"gab", "get all books"})
    public List<String> getAllBooks() {
        return bookService.getAllBooks().stream().map(Book::toString).collect(Collectors.toList());
    }

    @ShellMethod(value = "create book", key = {"cb", "create book"})
    public String createBook(@ShellOption("--title") String title, @ShellOption("--genre") String genre, @ShellOption(value = "--authors") String authors) {
        String[] authorNames = authors.split(";");
        Book createdBook = bookService.createBook(title, genre, authorNames);
        return String.format("Book with title %s was created, id: %d", createdBook.getTitle(), createdBook.getId());
    }

    @ShellMethod(value = "update book", key = {"ub", "update book"})
    public String updateBook(@ShellOption("--id") long id, @ShellOption("--title") String title, @ShellOption("--genre") String genre, @ShellOption(value = "--authors") String authors) {
        String[] authorNames = authors.split(";");
        try {
            Book updatedBook = bookService.updateBook(id, title, genre, authorNames);
            return String.format("Book with ID %d was updated: %s", id, updatedBook);
        } catch (RepositoryException e) {
            return e.getMessage();
        }
    }

    @ShellMethod(value = "delete book", key = {"db", "delete book"})
    public String deleteBook(@ShellOption("--id") long id) {
        try {
            bookService.deleteBook(id);
            return String.format("Book with ID %d was deleted.", id);
        } catch (RepositoryException e) {
            return e.getMessage();
        }
    }

    @ShellMethod(value = "get all comments for book", key = {"gc", "get comment"})
    public List<String> getCommentsByBook(@ShellOption("--bookId") long bookId) {
        try {
            List<CommentToBook> comments = commentToBookService.getAllCommentsToBook(bookId);
            if(comments.isEmpty()) {
                return List.of("There are no comments for book with id " + bookId);
            } else {
                return comments.stream().map(CommentToBook::toString).collect(Collectors.toList());
            }
        } catch (RepositoryException e) {
            return List.of(e.getMessage());
        }
    }

    @ShellMethod(value = "add comment to book", key = {"ac", "add comment"})
    public String addCommentToBook(@ShellOption("--bookId") long bookId, @ShellOption("--comment") String comment) {
        try {
            commentToBookService.addCommentToBook(bookId, comment);
            return String.format("Comment was added to book with id %d.", bookId);
        } catch (RepositoryException e) {
            return e.getMessage();
        }
    }

    @ShellMethod(value = "delete comment from book", key = {"dc", "delete comment"})
    public String deleteCommentFromBook(@ShellOption("--commentId") long commentId) {
        try {
            commentToBookService.deleteCommentFromBook(commentId);
            return String.format("Comment with id %d was removed from book.", commentId);
        } catch (RepositoryException e) {
            return e.getMessage();
        }
    }

    @ShellMethod(value = "get author", key = {"ga", "get author"})
    public String getAuthor(@ShellOption("--id") long id) {
        try {
            return "Requested author: " + authorService.getAuthor(id);
        } catch (RepositoryException e) {
            return e.getMessage();
        }
    }

    @ShellMethod(value = "create author", key = {"ca", "create author"})
    public String createAuthor(@ShellOption("--name") String name) {
        Author createdAuthor = authorService.createAuthor(name);
        return String.format("Author with name %s was created, id: %d", createdAuthor.getName(), createdAuthor.getId());
    }

    @ShellMethod(value = "update author", key = {"ua", "update author"})
    public String updateAuthor(@ShellOption("--id") long id, @ShellOption("--name") String name) {
        try {
            Author updatedAuthor = authorService.updateAuthor(id, name);
            return String.format("Author with id %d was updated: %s", id, updatedAuthor);
        } catch (RepositoryException e) {
            return e.getMessage();
        }
    }

    @ShellMethod(value = "delete author", key = {"da", "delete author"})
    public String deleteAuthor(@ShellOption("--id") long id) {
        try {
            authorService.deleteAuthor(id);
            return String.format("Author with ID %d was deleted.", id);
        } catch (RepositoryException e) {
            return e.getMessage();
        }
    }

    @ShellMethod(value = "get genre", key = {"gg", "get genre"})
    public String getGenre(@ShellOption("--id") long id) {
        try {
            return "Requested genre: " + genreService.getGenre(id).toString();
        } catch (RepositoryException e) {
            return e.getMessage();
        }
    }

    @ShellMethod(value = "create genre", key = {"cg", "create genre"})
    public String createGenre(@ShellOption("--name") String name) {
        Genre createdGenre = genreService.createGenre(name);
        return String.format("Genre with name %s was created, id: %d", createdGenre.getName(), createdGenre.getId());
    }

    @ShellMethod(value = "update genre", key = {"ug", "update genre"})
    public String updateGenre(@ShellOption("--id") long id, @ShellOption("--name") String name) {
        try {
            Genre updatedGenre = genreService.updateGenre(id, name);
            return String.format("Genre with id %d was updated: %s", id, updatedGenre);
        } catch (RepositoryException e) {
            return e.getMessage();
        }
    }
}
