package ru.otus.homework_6.repository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import ru.otus.homework_6.entity.Book;
import ru.otus.homework_6.entity.CommentToBook;
import ru.otus.homework_6.exception.RepositoryException;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

@DisplayName("Test BookRepository")
@DataJpaTest
@Import(BookRepositoryImpl.class)
public class BookRepositoryTest {

    private static final Long bookOneId = 1L;
    private static final Long bookTwoId = 2L;
    private static final int booksCount = 2;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private TestEntityManager em;

    @DisplayName("должен загружать список всех книг с названием и жанром")
    @Test
    void shouldReturnAllBooksWithTitlesAndGenres() {
        Book book1 = em.find(Book.class, bookOneId);
        Book book2 = em.find(Book.class, bookTwoId);
        List<Book> bookEntityList = bookRepository.getAll();
        assertThat(bookEntityList).hasSize(booksCount)
                .containsExactlyInAnyOrder(book1, book2)
                .allMatch(bookEntity -> bookEntity.getTitle() != null)
                .allMatch(bookEntity -> bookEntity.getGenre() != null);
    }

    @DisplayName("должен загружать заданную книгу с ее авторами")
    @Test
    void shouldReturnBookWithAuthors() {
        Book expectedBook = em.find(Book.class, bookOneId);
        Book actualBook = bookRepository.getById(bookOneId);
        assertThat(actualBook).isEqualTo(expectedBook)
                .matches(book -> book.getTitle() != null)
                .matches(book -> book.getGenre() != null)
                .matches(book -> book.getAuthors() != null && !book.getAuthors().isEmpty());
    }
}
