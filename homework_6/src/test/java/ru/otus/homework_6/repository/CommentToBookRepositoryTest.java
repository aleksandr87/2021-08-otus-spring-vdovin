package ru.otus.homework_6.repository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import ru.otus.homework_6.entity.Book;
import ru.otus.homework_6.entity.CommentToBook;
import ru.otus.homework_6.exception.RepositoryException;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Test CommentToBookRepository")
@DataJpaTest
@Import({CommentToBookRepositoryImpl.class, BookRepositoryImpl.class})
public class CommentToBookRepositoryTest {

    private static final Long bookOneId = 1L;
    private static final int commentsCountForBook1 = 3;
    private static final Long commentToDeleteId = 1L;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private CommentToBookRepository commentToBookRepository;

    @Autowired
    private TestEntityManager em;

    @DisplayName("должен загружать комментарии к заданной книге")
    @Test
    void shouldReturnCommentsToBook() throws RepositoryException {
        Book expectedBook = em.find(Book.class, bookOneId);
        List<CommentToBook> comments = commentToBookRepository.getAllByBook(bookOneId);
        assertThat(comments).hasSize(commentsCountForBook1)
                .allMatch(comment -> comment.getComment() != null)
                .allMatch(comment -> comment.getBook().equals(expectedBook));
    }

    @DisplayName("добавление комментария к книге")
    @Test
    void addCommentToBook() throws RepositoryException {
        CommentToBook commentToBook = new CommentToBook();
        commentToBook.setComment("New comment");
        Book actualBook = bookRepository.getById(bookOneId);
        commentToBook.setBook(actualBook);

        CommentToBook insertedComment = commentToBookRepository.insert(commentToBook);
        Book expectedBook = em.find(Book.class, bookOneId);
        assertThat(insertedComment.getBook().getId()).isEqualTo(expectedBook.getId());

        List<CommentToBook> comments = commentToBookRepository.getAllByBook(bookOneId);
        assertThat(comments).hasSize(commentsCountForBook1 + 1)
                .contains(commentToBook);
    }

    @DisplayName("удаление комментария книги")
    @Test
    void removeCommentFromBook() throws RepositoryException {
        CommentToBook commentToBook = commentToBookRepository.getById(commentToDeleteId).orElseThrow(
                () -> new RepositoryException(RepositoryException.COMMENT_ID_NOT_EXISTS, String.valueOf(commentToDeleteId))
        );
        commentToBookRepository.deleteFromBook(commentToBook);
        List<CommentToBook> comments = commentToBookRepository.getAllByBook(bookOneId);
        assertThat(comments).hasSize(commentsCountForBook1 - 1)
                .doesNotContain(commentToBook);
    }
}
