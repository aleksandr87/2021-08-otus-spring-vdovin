package ru.otus.homework_4.exception;

public class QuestionsSourceException extends Exception {
    public final static String NOT_FOUND = "File %s was not found!";
    public final static String WRONG_FORMAT = "File %s has wrong format!";

    private String errorMessage;
    private String fileName;

    public QuestionsSourceException(String errorMessage, String fileName) {
        this.errorMessage = errorMessage;
        this.fileName = fileName;
    }

    public String getMessage() {
        return String.format(errorMessage, fileName);
    }
}
