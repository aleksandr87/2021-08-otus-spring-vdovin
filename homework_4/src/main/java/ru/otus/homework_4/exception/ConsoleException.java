package ru.otus.homework_4.exception;

public class ConsoleException extends Exception {
    public final static String READ_INPUT_ERROR = "Something went wrong by reading input, please, try again!";

    private String errorMessage;

    public ConsoleException(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getMessage() {
        return errorMessage;
    }
}
