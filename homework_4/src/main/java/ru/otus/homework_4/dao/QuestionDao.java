package ru.otus.homework_4.dao;

import ru.otus.homework_4.domain.Question;
import ru.otus.homework_4.exception.QuestionsSourceException;

import java.util.List;

public interface QuestionDao {
    List<Question> getAllQuestions() throws QuestionsSourceException;
}
