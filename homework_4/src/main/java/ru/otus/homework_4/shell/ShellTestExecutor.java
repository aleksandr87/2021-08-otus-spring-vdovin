package ru.otus.homework_4.shell;

import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;
import ru.otus.homework_4.service.TestExecuteService;

@ShellComponent
public class ShellTestExecutor {

    private final TestExecuteService service;
    private String userName;

    public ShellTestExecutor(TestExecuteService service) {
        this.service = service;
    }

    @ShellMethod(value = "Login command", key = {"l", "login"})
    public String login() {
        userName = service.loginUser();
        return String.format("Welcome %s", userName);
    }

    @ShellMethod(value = "Execute test", key = {"s", "start"})
    @ShellMethodAvailability("availabilityCheck")
    public String test() {
        service.executeTest(userName);
        return "Good bye!";
    }

    public Availability availabilityCheck() {
        return (userName != null)
                ? Availability.available()
                : Availability.unavailable("Please, login first before test!");
    }
}
