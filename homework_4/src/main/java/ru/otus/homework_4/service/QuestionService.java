package ru.otus.homework_4.service;

import ru.otus.homework_4.domain.Question;
import ru.otus.homework_4.exception.QuestionsSourceException;

import java.util.List;

public interface QuestionService {
    List<Question> getContent() throws QuestionsSourceException;
}
