package ru.otus.homework_4.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.otus.homework_4.dao.QuestionDao;
import ru.otus.homework_4.domain.Question;
import ru.otus.homework_4.exception.QuestionsSourceException;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {

    private final QuestionDao dao;

    @Autowired
    public QuestionServiceImpl(QuestionDao dao) {
        this.dao = dao;
    }

    @Override
    public List<Question> getContent() throws QuestionsSourceException {
        return dao.getAllQuestions();
    }
}
