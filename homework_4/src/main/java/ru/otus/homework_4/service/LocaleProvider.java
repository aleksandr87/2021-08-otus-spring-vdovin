package ru.otus.homework_4.service;

import java.util.Locale;

public interface LocaleProvider {

    Locale getLocale();
}
