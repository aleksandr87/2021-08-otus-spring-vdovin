package ru.otus.homework_4.service;

public interface MessageSourceService extends InOutService {

    void printMessage(String message, String... messageArgs);
}
