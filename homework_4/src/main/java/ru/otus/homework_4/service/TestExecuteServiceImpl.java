package ru.otus.homework_4.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.otus.homework_4.config.AppProperties;
import ru.otus.homework_4.domain.Question;
import ru.otus.homework_4.enums.TestResultEnum;
import ru.otus.homework_4.exception.ConsoleException;
import ru.otus.homework_4.exception.QuestionsSourceException;

import java.util.List;

@Service
public class TestExecuteServiceImpl implements TestExecuteService {

    private final QuestionService questionService;
    private final AppProperties props;
    private final MessageSourceService messageSourceService;

    @Autowired
    TestExecuteServiceImpl(QuestionService questionService,
                           AppProperties props,
                           MessageSourceService messageSourceService) {
        this.questionService = questionService;
        this.props = props;
        this.messageSourceService = messageSourceService;
    }

    @Override
    public void executeTest(String userName) {
        int wrongAnswersCount = askQuestions();
        printResults(userName, wrongAnswersCount);
    }

    @Override
    public String loginUser() {
        return askStudentName();
    }

    private String askStudentName() {
        String studentName = "";
        messageSourceService.printMessage("message.ask.name");
        do {
            try {
                studentName = messageSourceService.readInput();
                if (studentName == null || studentName.isEmpty()) {
                    messageSourceService.printMessage("warning.emptyname");
                }
            } catch (ConsoleException e) {
                messageSourceService.printOutput(e.getMessage());
            }
        } while (studentName == null || studentName.isEmpty());
        return studentName;
    }

    private int askQuestions() {
        List<Question> questions;
        int wrongAnswersCount = 0;
        try {
            questions = questionService.getContent();
            for (int i = 0; i < questions.size(); i++) {
                Question question = questions.get(i);
                printQuestion(question);
                String studentsAnswer = messageSourceService.readInput();
                if (checkAnswerIsWrong(studentsAnswer, question.getRightAnswer())) wrongAnswersCount++;
            }
        } catch (QuestionsSourceException e) {
            messageSourceService.printOutput(e.getMessage());
            System.exit(1);
        } catch (ConsoleException e) {
            messageSourceService.printOutput(e.getMessage());
        }
        return wrongAnswersCount;
    }

    private void printQuestion(Question question) {
        String questionToPrint = String.format("%s", question.getQuestion());
        messageSourceService.printOutput(questionToPrint);
        List<String> answers = question.getAnswers();
        if (answers.size() > 0) {
            for (int j = 0; j < answers.size(); j++) {
                String answersToPrint = String.format("%d) %s; ", j + 1, answers.get(j));
                messageSourceService.printOutput(answersToPrint);
            }
        }
        messageSourceService.printMessage("message.ask.answer");
    }

    private boolean checkAnswerIsWrong(String studentsAnswer, String rightAnswer) {
        return !studentsAnswer.trim().equalsIgnoreCase(rightAnswer);
    }

    private void printResults(String studentName, int wrongAnswersNumber) {
        if (wrongAnswersNumber == 0) {
            messageSourceService.printMessage("message.result.exellent", studentName, TestResultEnum.PASSED.toString());
        } else if (wrongAnswersNumber <= props.getMaxAllowedWrongAnswers()) {
            messageSourceService.printMessage("message.result.success", studentName, TestResultEnum.PASSED.toString(), String.valueOf(wrongAnswersNumber));
        } else {
            messageSourceService.printMessage("message.result.failed", studentName, TestResultEnum.NOT_PASSED.toString(), String.valueOf(wrongAnswersNumber));
        }
    }
}
