package ru.otus.homework_4.service;

public interface ResourceProvider {

    String getCsvFile();
}
