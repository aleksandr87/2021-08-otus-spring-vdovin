package ru.otus.homework_4.service;

import ru.otus.homework_4.exception.ConsoleException;

public interface InOutService {
    void printOutput(String output);

    String readInput() throws ConsoleException;
}
