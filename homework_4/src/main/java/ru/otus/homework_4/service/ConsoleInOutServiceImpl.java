package ru.otus.homework_4.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.otus.homework_4.exception.ConsoleException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

@Service
public class ConsoleInOutServiceImpl implements InOutService {

    private final PrintStream printStream;
    private final BufferedReader bufferedReader;

    public ConsoleInOutServiceImpl(@Value("#{ T(java.lang.System).out}") PrintStream printStream,
                                   @Value("#{ T(java.lang.System).in}") InputStream inputStream) {
        this.printStream = printStream;
        this.bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
    }

    @Override
    public void printOutput(String output) {
        printStream.println(output);
    }

    @Override
    public String readInput() throws ConsoleException {
        String input = "";
        try {
            input = bufferedReader.readLine();
        } catch (IOException e) {
            throw new ConsoleException(ConsoleException.READ_INPUT_ERROR);
        }
        return input;
    }
}
