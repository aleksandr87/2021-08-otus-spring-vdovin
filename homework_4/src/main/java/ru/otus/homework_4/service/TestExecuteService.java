package ru.otus.homework_4.service;

public interface TestExecuteService {

    void executeTest(String userName);

    String loginUser();
}
