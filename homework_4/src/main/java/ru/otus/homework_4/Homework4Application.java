package ru.otus.homework_4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import ru.otus.homework_4.config.AppProperties;
import ru.otus.homework_4.service.TestExecuteService;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class Homework4Application {

    public static void main(String[] args) {
        SpringApplication.run(Homework4Application.class, args);
    }
}
