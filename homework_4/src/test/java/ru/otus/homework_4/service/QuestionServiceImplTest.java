package ru.otus.homework_4.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.otus.homework_4.dao.QuestionDao;
import ru.otus.homework_4.domain.Question;
import ru.otus.homework_4.exception.QuestionsSourceException;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

@SpringBootTest
public class QuestionServiceImplTest {

    private static final String FILE_NAME = "someFile.csv";
    private static final String SOURCE_NOT_FOUND_MESSAGE = String.format(QuestionsSourceException.NOT_FOUND, FILE_NAME);
    private static final String SOURCE_HAS_WRONG_FORMAT = String.format(QuestionsSourceException.WRONG_FORMAT, FILE_NAME);

    @MockBean
    private QuestionDao questionDao;

    @Autowired
    private QuestionServiceImpl service;

    private List<Question> questionList = new ArrayList<>();
    private List<Question> emptyQuestionList = new ArrayList<>();

    @BeforeEach
    void setUp() {
        List<String> answerList = new ArrayList<>();
        answerList.add("answer 1");
        Question question = new Question("question 1", answerList, "right answer");
        questionList.add(question);
    }

    @Test
    public void testGetContent() throws QuestionsSourceException {
        given(questionDao.getAllQuestions()).willReturn(questionList);
        assertThat(service.getContent()).hasSize(1);
        assertThat(questionList.get(0).getQuestion()).isEqualTo("question 1");
        assertThat(questionList.get(0).getAnswers().get(0)).isEqualTo("answer 1");
        assertThat(questionList.get(0).getRightAnswer()).isEqualTo("right answer");
    }

    @Test
    public void testGetEmptyContent() throws QuestionsSourceException {
        given(questionDao.getAllQuestions()).willReturn(emptyQuestionList);
        assertThat(service.getContent()).hasSize(0);
    }

    @Test
    public void testSourceNotFound() throws QuestionsSourceException {
        given(questionDao.getAllQuestions()).willThrow(new QuestionsSourceException(QuestionsSourceException.NOT_FOUND, FILE_NAME));
        Exception exception = assertThrows(QuestionsSourceException.class, () -> service.getContent());
        assertThat(exception.getMessage()).isEqualTo(SOURCE_NOT_FOUND_MESSAGE);
    }

    @Test
    public void testSourceHasWrongFormat() throws QuestionsSourceException {
        given(questionDao.getAllQuestions()).willThrow(new QuestionsSourceException(QuestionsSourceException.WRONG_FORMAT, FILE_NAME));
        Exception exception = assertThrows(QuestionsSourceException.class, () -> service.getContent());
        assertThat(exception.getMessage()).isEqualTo(SOURCE_HAS_WRONG_FORMAT);
    }
}
