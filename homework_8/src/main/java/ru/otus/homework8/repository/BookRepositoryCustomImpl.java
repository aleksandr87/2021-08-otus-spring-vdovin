package ru.otus.homework8.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import ru.otus.homework8.entity.CommentToBook;

public class BookRepositoryCustomImpl implements BookRepositoryCustom{

    private final MongoTemplate mongoTemplate;

    public BookRepositoryCustomImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public void removeCommentsByBookId(String id) {
        Query query = Query.query(Criteria.where("book.$id").is(new ObjectId(id)));
        mongoTemplate.findAllAndRemove(query, CommentToBook.class, "comments");
    }
}
