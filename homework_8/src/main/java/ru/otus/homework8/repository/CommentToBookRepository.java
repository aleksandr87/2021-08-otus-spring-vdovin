package ru.otus.homework8.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.otus.homework8.entity.Book;
import ru.otus.homework8.entity.CommentToBook;

import java.util.List;

public interface CommentToBookRepository extends MongoRepository<CommentToBook, String> {

    List<CommentToBook> findByBook(Book book);

}
