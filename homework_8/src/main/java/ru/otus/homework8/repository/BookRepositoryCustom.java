package ru.otus.homework8.repository;

public interface BookRepositoryCustom {

    void removeCommentsByBookId(String id);
}
