package ru.otus.homework8.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.otus.homework8.entity.Book;

import java.util.List;

public interface BookRepository extends MongoRepository<Book, String>, BookRepositoryCustom {

    List<Book> findByTitleRegex(String title);
}
