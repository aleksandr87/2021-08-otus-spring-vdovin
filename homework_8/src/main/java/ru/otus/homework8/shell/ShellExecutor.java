package ru.otus.homework8.shell;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ru.otus.homework8.entity.Book;
import ru.otus.homework8.entity.CommentToBook;
import ru.otus.homework8.exception.RepositoryException;
import ru.otus.homework8.service.BookService;
import ru.otus.homework8.service.CommentToBookService;

import java.util.List;
import java.util.stream.Collectors;

@ShellComponent
public class ShellExecutor {

    private final BookService bookService;
    private final CommentToBookService commentToBookService;

    public ShellExecutor(BookService bookService, CommentToBookService commentToBookService) {
        this.bookService = bookService;
        this.commentToBookService = commentToBookService;
    }

    @ShellMethod(value = "get book", key = {"gb", "get book"})
    public String getBook(@ShellOption("--id") String id) {
        try {
            return "Requested book: " + bookService.getBook(id);
        } catch (RepositoryException e) {
            return e.getMessage();
        }
    }

    @ShellMethod(value = "get book by title", key = {"gbbt", "get book by title"})
    public String getBookByTitle(@ShellOption("--title") String title) {
        List<Book> foundBooks = bookService.getBookByTitle(title);
        if(foundBooks.isEmpty()) {
            return String.format("Book with title %s was not found", title);
        }
        return "Founded books: " + foundBooks;
    }

    @ShellMethod(value = "get all books", key = {"gab", "get all books"})
    public List<String> getAllBooks() {
        return bookService.getAllBooks().stream().map(Book::toString).collect(Collectors.toList());
    }

    @ShellMethod(value = "create book", key = {"cb", "create book"})
    public String createBook(@ShellOption("--title") String title, @ShellOption("--genre") String genre, @ShellOption(value = "--authors") String authors) {
        String[] authorNames = authors.split(";");
        Book createdBook = bookService.createBook(title, genre, authorNames);
        return String.format("Book with title %s was created, id: %s", createdBook.getTitle(), createdBook.getId());
    }

    @ShellMethod(value = "update book", key = {"ub", "update book"})
    public String updateBook(@ShellOption("--id") String id, @ShellOption("--title") String title, @ShellOption("--genre") String genre, @ShellOption(value = "--authors") String authors) {
        String[] authorNames = authors.split(";");
        try {
            Book updatedBook = bookService.updateBook(id, title, genre, authorNames);
            return String.format("Book with ID %s was updated: %s", id, updatedBook);
        } catch (RepositoryException e) {
            return e.getMessage();
        }
    }

    @ShellMethod(value = "delete book", key = {"db", "delete book"})
    public String deleteBook(@ShellOption("--id") String id) {
        try {
            bookService.deleteBook(id);
            return String.format("Book with ID %s was deleted.", id);
        } catch (RepositoryException e) {
            return e.getMessage();
        }
    }

    @ShellMethod(value = "get all comments for book", key = {"gc", "get comment"})
    public List<String> getCommentsByBook(@ShellOption("--bookId") String bookId) {
        try {
            List<CommentToBook> comments = commentToBookService.getAllCommentsToBook(bookId);
            if(comments.isEmpty()) {
                return List.of("There are no comments for book with id " + bookId);
            } else {
                return comments.stream().map(CommentToBook::toString).collect(Collectors.toList());
            }
        } catch (RepositoryException e) {
            return List.of(e.getMessage());
        }
    }

    @ShellMethod(value = "add comment to book", key = {"ac", "add comment"})
    public String addCommentToBook(@ShellOption("--bookId") String bookId, @ShellOption("--comment") String comment) {
        try {
            commentToBookService.addCommentToBook(bookId, comment);
            return String.format("Comment was added to book with id %s.", bookId);
        } catch (RepositoryException e) {
            return e.getMessage();
        }
    }

    @ShellMethod(value = "delete comment from book", key = {"dc", "delete comment"})
    public String deleteCommentFromBook(@ShellOption("--commentId") String commentId) {
        try {
            commentToBookService.deleteCommentFromBook(commentId);
            return String.format("Comment with id %s was removed from book.", commentId);
        } catch (RepositoryException e) {
            return e.getMessage();
        }
    }
}
