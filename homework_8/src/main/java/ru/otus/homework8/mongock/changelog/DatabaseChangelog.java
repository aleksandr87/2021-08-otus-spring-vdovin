package ru.otus.homework8.mongock.changelog;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.mongodb.client.MongoDatabase;
import ru.otus.homework8.entity.Author;
import ru.otus.homework8.entity.Book;
import ru.otus.homework8.entity.CommentToBook;
import ru.otus.homework8.entity.Genre;
import ru.otus.homework8.repository.BookRepository;
import ru.otus.homework8.repository.CommentToBookRepository;

import java.util.HashSet;
import java.util.Set;

@ChangeLog
public class DatabaseChangelog {

    @ChangeSet(order = "001", id = "dropDb", runAlways = true, author = "vav")
    public void dropDb(MongoDatabase db) {
        db.drop();
    }

    @ChangeSet(order = "002", id = "insertBook1", author = "vav")
    public void insertBook1(BookRepository bookRepository, CommentToBookRepository commentToBookRepository) {
        Genre genre = new Genre();
        genre.setName("education");
        Set<Author> authors = new HashSet<>();
        Author author1 = new Author();
        author1.setName("Benjamin J Evans");
        authors.add(author1);
        Author author2 = new Author();
        author2.setName("David Flanagan");
        authors.add(author2);
        Book book1 = new Book();
        book1.setTitle("Java in a Nutshell");
        book1.setGenre(genre);
        book1.setAuthors(authors);
        bookRepository.save(book1);

        CommentToBook comment1 = new CommentToBook();
        comment1.setComment("Useful book");
        comment1.setBook(book1);
        commentToBookRepository.save(comment1);

        CommentToBook comment2 = new CommentToBook();
        comment2.setComment("Boring book");
        comment2.setBook(book1);
        commentToBookRepository.save(comment2);

        CommentToBook comment3 = new CommentToBook();
        comment3.setComment("Worth to read");
        comment3.setBook(book1);
        commentToBookRepository.save(comment3);

        CommentToBook comment4 = new CommentToBook();
        comment4.setComment("Dont waste your time for it");
        comment4.setBook(book1);
        commentToBookRepository.save(comment4);
    }

    @ChangeSet(order = "003", id = "insertBook2", author = "vav")
    public void insertBook2(BookRepository repository) {
        Genre genre = new Genre();
        genre.setName("politics");
        Author author = new Author();
        author.setName("Lee Kuan Yew");
        Book book2 = new Book();
        book2.setTitle("From Third World to First: The Singapore Story");
        book2.setGenre(genre);
        book2.setAuthors(Set.of(author));
        repository.save(book2);
    }

    @ChangeSet(order = "004", id = "insertBook3", author = "vav")
    public void insertBook3(BookRepository repository) {
        Genre genre = new Genre();
        genre.setName("education");
        Author author1 = new Author();
        author1.setName("Scott Chacon");
        Author author2 = new Author();
        author2.setName("Ben Straub");
        Set<Author> authors = new HashSet<>();
        authors.add(author1);
        authors.add(author2);
        Book book3 = new Book();
        book3.setTitle("Pro Git");
        book3.setGenre(genre);
        book3.setAuthors(authors);
        repository.save(book3);
    }

    @ChangeSet(order = "005", id = "insertBook4", author = "vav")
    public void insertBook4(BookRepository repository) {
        Genre genre = new Genre();
        genre.setName("education");
        Author author1 = new Author();
        author1.setName("Erich Gamma");
        Author author2 = new Author();
        author2.setName("Richard Helm");
        Author author3 = new Author();
        author3.setName("Ralph Johnson");
        Author author4 = new Author();
        author4.setName("John Vlissides");
        Set<Author> authors = new HashSet<>();
        authors.add(author1);
        authors.add(author2);
        authors.add(author3);
        authors.add(author4);
        Book book4 = new Book();
        book4.setTitle("Design Patterns. Elements of Reusable Object-Oriented Software");
        book4.setGenre(genre);
        book4.setAuthors(authors);
        repository.save(book4);
    }

    @ChangeSet(order = "006", id = "insertBook5", author = "vav")
    public void insertBook5(BookRepository repository) {
        Genre genre = new Genre();
        genre.setName("education");
        Author author = new Author();
        author.setName("Joshua Bloch");
        Book book5 = new Book();
        book5.setTitle("Effective Java");
        book5.setGenre(genre);
        book5.setAuthors(Set.of(author));
        repository.save(book5);
    }

    @ChangeSet(order = "007", id = "insertBook6", author = "vav")
    public void insertBook6(BookRepository repository) {
        Genre genre = new Genre();
        genre.setName("education");
        Author author = new Author();
        author.setName("Giuseppe Bonaccorso");
        Book book6 = new Book();
        book6.setTitle("Machine Learning Algorithms");
        book6.setGenre(genre);
        book6.setAuthors(Set.of(author));
        repository.save(book6);
    }

    @ChangeSet(order = "008", id = "insertBook7", author = "vav")
    public void insertBook7(BookRepository repository) {
        Genre genre = new Genre();
        genre.setName("education");
        Author author = new Author();
        author.setName("Chris Richardson");
        Book book7 = new Book();
        book7.setTitle("Microservices Patterns");
        book7.setGenre(genre);
        book7.setAuthors(Set.of(author));
        repository.save(book7);
    }

    @ChangeSet(order = "009", id = "insertBook8", author = "vav")
    public void insertBook8(BookRepository repository) {
        Genre genre = new Genre();
        genre.setName("Sci-Fi");
        Author author = new Author();
        author.setName("James S. A. Corey");
        Book book8 = new Book();
        book8.setTitle("Leviathan Wakes");
        book8.setGenre(genre);
        book8.setAuthors(Set.of(author));
        repository.save(book8);
    }
}
