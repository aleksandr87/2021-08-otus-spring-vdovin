package ru.otus.homework8.service;

import ru.otus.homework8.entity.CommentToBook;
import ru.otus.homework8.exception.RepositoryException;

import java.util.List;

public interface CommentToBookService {

    List<CommentToBook> getAllCommentsToBook(String bookId) throws RepositoryException;

    void addCommentToBook(String bookId, String comment) throws RepositoryException;

    void deleteCommentFromBook(String id) throws RepositoryException;
}
