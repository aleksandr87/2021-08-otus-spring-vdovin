package ru.otus.homework8.service;

import ru.otus.homework8.entity.Book;
import ru.otus.homework8.exception.RepositoryException;

import java.util.List;

public interface BookService {

    Book createBook(String title, String genre, String[] authors);

    Book getBook(String id) throws RepositoryException;

    List<Book> getBookByTitle(String title);

    List<Book> getAllBooks();

    Book updateBook(String id, String title, String genre, String[] authors) throws RepositoryException;

    void deleteBook(String id) throws RepositoryException;
}
