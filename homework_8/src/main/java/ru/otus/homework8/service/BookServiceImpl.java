package ru.otus.homework8.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.otus.homework8.entity.Author;
import ru.otus.homework8.entity.Book;
import ru.otus.homework8.entity.Genre;
import ru.otus.homework8.exception.RepositoryException;
import ru.otus.homework8.repository.BookRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;;
    }

    @Override
    public Book createBook(String title, String genreName, String[] authors) {
        Genre genre = new Genre(genreName);
        Set<Author> authorsSet = new HashSet<>();
        for(String authorName: authors) {
            Author author = new Author(authorName);
            authorsSet.add(author);
        }
        Book bookEntity = new Book();
        bookEntity.setTitle(title);
        bookEntity.setGenre(genre);
        bookEntity.setAuthors(authorsSet);
        return bookRepository.save(bookEntity);
    }

    @Override
    public Book getBook(String id) throws RepositoryException {
        Book bookEntity = bookRepository.findById(id).orElseThrow(
                () -> new RepositoryException(RepositoryException.BOOK_ID_NOT_EXISTS, id)
        );
        return bookEntity;
    }

    public List<Book> getBookByTitle(String title) {
        List<Book> foundBooks = bookRepository.findByTitleRegex(title);
        return foundBooks;
    }

    @Override
    public List<Book> getAllBooks() {
        List<Book> bookEntities = bookRepository.findAll();
        return bookEntities;
    }

    @Override
    public Book updateBook(String id, String title, String genreName, String[] authors) throws RepositoryException {
        Book bookEntity = bookRepository.findById(id).orElseThrow(
                () -> new RepositoryException(RepositoryException.BOOK_ID_NOT_EXISTS, id)
        );
        if(title != null) {
            bookEntity.setTitle(title);
        }
        if(genreName != null) {
            Genre genre = new Genre(genreName);
            bookEntity.setGenre(genre);
        }
        if(authors.length > 0) {
            Set<Author> authorsSet = new HashSet<>();
            for(String authorName: authors) {
                Author author = new Author(authorName);
                authorsSet.add(author);
            }
            bookEntity.setAuthors(authorsSet);
        }
        return bookRepository.save(bookEntity);
    }

    @Override
    public void deleteBook(String id) throws RepositoryException {
        if (bookRepository.existsById(id)){
            bookRepository.removeCommentsByBookId(id);
            bookRepository.deleteById(id);
        } else {
            throw new RepositoryException(RepositoryException.BOOK_ID_NOT_EXISTS, id);
        }
    }
}
