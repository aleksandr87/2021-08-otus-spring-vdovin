package ru.otus.homework8.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.otus.homework8.entity.Book;
import ru.otus.homework8.entity.CommentToBook;
import ru.otus.homework8.exception.RepositoryException;
import ru.otus.homework8.repository.CommentToBookRepository;

import java.util.List;

@Service
public class CommentToBookServiceImpl implements CommentToBookService {

    private final CommentToBookRepository commentToBookRepository;
    private final BookService bookService;

    @Autowired
    CommentToBookServiceImpl(CommentToBookRepository commentToBookRepository, BookService bookService) {
        this.commentToBookRepository = commentToBookRepository;
        this.bookService = bookService;
    }

    @Override
    public List<CommentToBook> getAllCommentsToBook(String bookId) throws RepositoryException {
        Book book = bookService.getBook(bookId);
        return commentToBookRepository.findByBook(book);
    }

    @Override
    public void addCommentToBook(String bookId, String comment) throws RepositoryException {
        Book book = bookService.getBook(bookId);
        CommentToBook commentToBook = new CommentToBook();
        commentToBook.setBook(book);
        commentToBook.setComment(comment);
        commentToBookRepository.save(commentToBook);
    }

    @Override
    public void deleteCommentFromBook(String id) throws RepositoryException {
        if(commentToBookRepository.existsById(id)) {
            commentToBookRepository.deleteById(id);
        } else {
            throw new RepositoryException(RepositoryException.COMMENT_ID_NOT_EXISTS, id);
        }
    }
}
