package ru.otus.homework8.entity;

import java.util.Objects;

public class Genre {

    private String name;

    public Genre() {}

    public Genre(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return String.format("NAME: %s", name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Genre))
            return false;
        Genre entity = (Genre) obj;
        return this.name.equals(entity.getName());
    }
}
