package ru.otus.homework8.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "comments")
public class CommentToBook {

    @Id
    private String id;

    private String comment;

    @DBRef
    private Book book;

    public CommentToBook() {}

    public CommentToBook(String id, String comment, Book book) {
        this.id = id;
        this.comment = comment;
        this.book = book;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public String toString() {
        return String.format("ID: %s; COMMENT: %s", id, comment);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof CommentToBook))
            return false;
        CommentToBook entity = (CommentToBook) obj;
        return (this.id.equals(entity.getId()) && this.comment.equals(entity.getComment()));
    }
}
