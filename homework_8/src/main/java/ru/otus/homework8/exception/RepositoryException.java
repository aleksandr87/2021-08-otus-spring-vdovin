package ru.otus.homework8.exception;

public class RepositoryException extends Exception{

    public static final String BOOK_ID_NOT_EXISTS = "Book with ID %s doesn't exist";
    public static final String COMMENT_ID_NOT_EXISTS = "Comment with ID %s doesn't exist";

    private final String errorMessage;
    private final String queryParam;

    public RepositoryException(String errorMessage, String queryParam) {
        this.errorMessage = errorMessage;
        this.queryParam = queryParam;
    }

    public RepositoryException(String errorMessage, String queryParam, Throwable cause) {
        super(cause);
        this.errorMessage = errorMessage;
        this.queryParam = queryParam;
    }

    public String getMessage() {
        return String.format(errorMessage, queryParam) + (getCause() != null ? ": "+getCause() : "");
    }
}
