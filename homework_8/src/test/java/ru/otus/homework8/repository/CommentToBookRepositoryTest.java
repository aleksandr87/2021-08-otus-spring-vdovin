package ru.otus.homework8.repository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import ru.otus.homework8.entity.Author;
import ru.otus.homework8.entity.Book;
import ru.otus.homework8.entity.CommentToBook;
import ru.otus.homework8.entity.Genre;

import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Test CommentToBookRepository")
@DataMongoTest
public class CommentToBookRepositoryTest {

    private static final String TEST_BOOK_TITLE = "Test Book";
    private static final String TEST_BOOK_GENRE = "Test Genre";
    private static final String TEST_BOOK_AUTHOR = "Test Author";
    private static final String TEST_COMMENT_1 = "Test Comment 1";
    private static final String TEST_COMMENT_2 = "Test Comment 2";

    @Autowired
    CommentToBookRepository commentToBookRepository;

    @Autowired
    private BookRepository bookRepository;

    @AfterEach
    void cleanUp() {
        bookRepository.deleteAll();
        commentToBookRepository.deleteAll();
    }

    @DisplayName("должен загружать комментарии к заданной книге")
    @Test
    public void shouldReturnCommentsToBook() {
        Genre genre = new Genre();
        genre.setName(TEST_BOOK_GENRE);
        Author author = new Author();
        author.setName(TEST_BOOK_AUTHOR);
        Book book = new Book();
        book.setTitle(TEST_BOOK_TITLE);
        book.setGenre(genre);
        book.setAuthors(Set.of(author));
        bookRepository.save(book);

        CommentToBook comment1 = new CommentToBook();
        comment1.setBook(book);
        comment1.setComment(TEST_COMMENT_1);
        commentToBookRepository.save(comment1);

        CommentToBook comment2 = new CommentToBook();
        comment2.setBook(book);
        comment2.setComment(TEST_COMMENT_2);
        commentToBookRepository.save(comment2);

        List<CommentToBook> foundComments = commentToBookRepository.findByBook(book);
        assertThat(foundComments).hasSize(2).contains(comment1).contains(comment2);
    }

    @DisplayName("удаление комментария книги")
    @Test
    public void addCommentToBook() {
        Genre genre = new Genre();
        genre.setName(TEST_BOOK_GENRE);
        Author author = new Author();
        author.setName(TEST_BOOK_AUTHOR);
        Book book = new Book();
        book.setTitle(TEST_BOOK_TITLE);
        book.setGenre(genre);
        book.setAuthors(Set.of(author));
        bookRepository.save(book);

        CommentToBook comment1 = new CommentToBook();
        comment1.setBook(book);
        comment1.setComment(TEST_COMMENT_1);
        commentToBookRepository.save(comment1);

        CommentToBook comment2 = new CommentToBook();
        comment2.setBook(book);
        comment2.setComment(TEST_COMMENT_2);
        commentToBookRepository.save(comment2);

        commentToBookRepository.delete(comment1);
        List<CommentToBook> foundComments = commentToBookRepository.findByBook(book);
        assertThat(foundComments).hasSize(1).doesNotContain(comment1);
    }

}
