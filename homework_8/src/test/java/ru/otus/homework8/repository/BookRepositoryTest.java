package ru.otus.homework8.repository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import ru.otus.homework8.entity.Author;
import ru.otus.homework8.entity.Book;
import ru.otus.homework8.entity.Genre;

import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Test BookRepository")
@DataMongoTest
public class BookRepositoryTest {

    private static final int BOOKS_AMOUNT = 1;
    private static final String TEST_BOOK_TITLE = "Test Book";
    private static final String TEST_BOOK_GENRE = "Test Genre";
    private static final String TEST_BOOK_AUTHOR = "Test Author";

    private static final String BOOK_TO_ADD_TITLE = "Book to add";
    private static final String BOOK_TO_ADD_GENRE = "Genre to add";
    private static final String BOOK_TO_ADD_AUTHOR = "Author to add";

    @Autowired
    private BookRepository bookRepository;

    @AfterEach
    void cleanUp() {
        bookRepository.deleteAll();
    }

    @DisplayName("поиск книги")
    @Test
    public void getBook() {
        Genre genre = new Genre();
        genre.setName(TEST_BOOK_GENRE);
        Author author = new Author();
        author.setName(TEST_BOOK_AUTHOR);
        Book book = new Book();
        book.setTitle(TEST_BOOK_TITLE);
        book.setGenre(genre);
        book.setAuthors(Set.of(author));
        bookRepository.save(book);

        List<Book> foundBooks = bookRepository.findByTitleRegex(TEST_BOOK_TITLE);
        Book foundBook = foundBooks.get(0);
        assertThat(foundBook).isEqualTo(book);
    }

    @DisplayName("добавление книги")
    @Test
    public void addNewBook() {
        Genre genre = new Genre();
        genre.setName(BOOK_TO_ADD_GENRE);
        Author author = new Author();
        author.setName(BOOK_TO_ADD_AUTHOR);
        Book book = new Book();
        book.setTitle(BOOK_TO_ADD_TITLE);
        book.setGenre(genre);
        book.setAuthors(Set.of(author));
        bookRepository.save(book);

        List<Book> foundBooks = bookRepository.findByTitleRegex(book.getTitle());

        assertThat(foundBooks).hasSize(BOOKS_AMOUNT).contains(book);
    }

    @DisplayName("удаление книги")
    @Test
    public void deleteBook() {
        Genre genre = new Genre();
        genre.setName(TEST_BOOK_GENRE);
        Author author = new Author();
        author.setName(TEST_BOOK_AUTHOR);
        Book book = new Book();
        book.setTitle(TEST_BOOK_TITLE);
        book.setGenre(genre);
        book.setAuthors(Set.of(author));
        bookRepository.save(book);

        List<Book> books = bookRepository.findByTitleRegex(TEST_BOOK_TITLE);
        Book bookToDelete = books.get(0);
        bookRepository.delete(bookToDelete);

        List <Book> foundBooks = bookRepository.findAll();

        assertThat(foundBooks).doesNotContain(bookToDelete);
    }
}
