package ru.otus.homework_7.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.otus.homework_7.entity.Genre;

import java.util.Optional;

public interface GenreRepository extends JpaRepository<Genre, Long> {

    Optional<Genre> findByName(String s);
}
