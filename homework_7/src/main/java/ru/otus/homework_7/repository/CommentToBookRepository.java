package ru.otus.homework_7.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.otus.homework_7.entity.CommentToBook;

import java.util.List;

public interface CommentToBookRepository extends JpaRepository<CommentToBook, Long> {

    List<CommentToBook> getAllByBookId(Long id);

}
