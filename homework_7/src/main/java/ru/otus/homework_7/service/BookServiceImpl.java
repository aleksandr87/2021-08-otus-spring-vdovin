package ru.otus.homework_7.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.otus.homework_7.entity.Author;
import ru.otus.homework_7.entity.Book;
import ru.otus.homework_7.entity.Genre;
import ru.otus.homework_7.exception.RepositoryException;
import ru.otus.homework_7.repository.BookRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class BookServiceImpl implements BookService{

    private final BookRepository bookRepository;
    private final GenreService genreService;
    private final AuthorService authorService;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository, GenreService genreService, AuthorService authorService) {
        this.bookRepository = bookRepository;
        this.genreService = genreService;
        this.authorService = authorService;
    }

    @Override
    @Transactional
    public Book createBook(String title, String genreName, String[] authors) {
        Book bookEntity = getEntityToInsert(title, genreName, authors);
        return bookRepository.save(bookEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Book getBook(long id) throws RepositoryException {
        Book bookEntity = bookRepository.findById(id).orElseThrow(
                () -> new RepositoryException(RepositoryException.BOOK_ID_NOT_EXISTS, String.valueOf(id))
        );
        return getBookDTO(bookEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Book> getAllBooks() {
        List<Book> bookEntities = bookRepository.findAll();
        return bookEntities.stream().map(this::getBookDTO).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Book updateBook(long id, String title, String genreName, String[] authors) throws RepositoryException {
        Book bookEntity = getEntityToUpdate(id, title, genreName, authors);
        return bookRepository.save(bookEntity);
    }

    @Override
    @Transactional
    public void deleteBook(long id) throws RepositoryException {
        Book bookEntity = getBook(id);
        bookRepository.delete(bookEntity);
    }

    private Book getEntityToInsert(String title, String genreName, String[] authors) {
        Book bookEntity = new Book();
        bookEntity.setTitle(title);
        Genre genre = null;
        try {
            genre = genreService.getByName(genreName);
        } catch (RepositoryException e) {
            genre = genreService.createGenre(genreName);
        }
        bookEntity.setGenre(genre);
        Set<Author> authorsSet = new HashSet<>();
        for(String authorName: authors) {
            Author author = null;
            try {
                author = authorService.getByName(authorName);
            } catch (RepositoryException e) {
                author = authorService.createAuthor(authorName);
            }
            author.setName(authorName);
            authorsSet.add(author);
        }
        bookEntity.setAuthors(authorsSet);
        return bookEntity;
    }

    private Book getEntityToUpdate(long id, String title, String genreName, String[] authors) throws RepositoryException {
        Book bookEntity = getBook(id);
        if (title != null) bookEntity.setTitle(title);
        if (genreName != null) {
            Genre genre = null;
            try {
                genre = genreService.getByName(genreName);
            } catch (RepositoryException e) {
                genre = genreService.createGenre(genreName);
            }
            bookEntity.setGenre(genre);
        }
        Set<Author> authorsSet = new HashSet<>();
        for(String authorName: authors) {
            Author author = null;
            try {
                author = authorService.getByName(authorName);
            } catch (RepositoryException e) {
                author = authorService.createAuthor(authorName);
            }
            author.setName(authorName);
            authorsSet.add(author);
        }
        bookEntity.setAuthors(authorsSet);
        return bookEntity;
    }

    private Book getBookDTO(Book book) {
        Set<Author> authors = book.getAuthors().stream()
                .map(author -> new Author(author.getId(), author.getName()))
                .collect(Collectors.toSet());
        Genre genre = new Genre(book.getGenre().getId(), book.getGenre().getName());
        return new Book(book.getId(), book.getTitle(), authors, genre);
    }
}
