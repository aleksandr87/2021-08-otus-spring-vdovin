package ru.otus.homework_7.repository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import ru.otus.homework_7.entity.Book;
import ru.otus.homework_7.exception.RepositoryException;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Test CommentToBookRepository")
@DataJpaTest
public class BookRepositoryTest {

    private static final Long bookOneId = 1L;
    private static final Long bookTwoId = 2L;
    private static final String insertedBook = "New book";
    private static final int amountOfBooks = 2;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private TestEntityManager em;

    @DisplayName("добавление книги")
    @Test
    public void addNewBook() {
        Book newBook = new Book();
        newBook.setTitle(insertedBook);
        bookRepository.save(newBook);

        Book requestedBook = em.find(Book.class, newBook.getId());
        assertThat(newBook.getTitle()).isEqualTo(requestedBook.getTitle());
    }

    @DisplayName("удаление книги")
    @Test
    public void deleteBook() throws RepositoryException {
        Book newBook = new Book();
        newBook.setTitle(insertedBook);
        this.em.merge(newBook);
        Book bookToDelete = bookRepository.findByTitle(insertedBook)
                .orElseThrow(() -> new RepositoryException(RepositoryException.BOOK_TITLE_NOT_EXISTS, insertedBook));

        bookRepository.delete(bookToDelete);
        List<Book> books = bookRepository.findAll();
        assertThat(books).doesNotContain(bookToDelete);
    }

    @DisplayName("должен вернуть все книги")
    @Test
    public void getAllBooks() {
        Book book1 = em.find(Book.class, bookOneId);
        Book book2 = em.find(Book.class, bookTwoId);
        List<Book> foundBooks = bookRepository.findAll();
        assertThat(foundBooks).hasSize(amountOfBooks)
                .containsExactlyInAnyOrder(book1, book2)
                .allMatch(bookEntity -> bookEntity.getTitle() != null)
                .allMatch(bookEntity -> bookEntity.getGenre() != null);
    }
}
