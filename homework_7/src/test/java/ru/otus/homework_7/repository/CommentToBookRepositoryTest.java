package ru.otus.homework_7.repository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import ru.otus.homework_7.entity.Book;
import ru.otus.homework_7.entity.CommentToBook;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Test CommentToBookRepository")
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class CommentToBookRepositoryTest {

    private static final Long bookOneId = 1L;
    private static final int commentsCountForBook1 = 3;
    private static final Long commentToDeleteId = 1L;

    @Autowired
    private CommentToBookRepository commentToBookRepository;

    @Autowired
    private TestEntityManager em;

    @DisplayName("должен загружать комментарии к заданной книге")
    @Test
    void shouldReturnCommentsToBook() {
        Book expectedBook = em.find(Book.class, bookOneId);
        List<CommentToBook> comments = commentToBookRepository.getAllByBookId(bookOneId);
        assertThat(comments).hasSize(commentsCountForBook1)
                .allMatch(comment -> comment.getComment() != null)
                .allMatch(comment -> comment.getBook().equals(expectedBook));
    }

    @DisplayName("добавление комментария к книге")
    @Test
    void addCommentToBook() {
        CommentToBook commentToBook = new CommentToBook();
        commentToBook.setComment("New comment");
        Book actualBook = em.find(Book.class, bookOneId);
        commentToBook.setBook(actualBook);

        CommentToBook insertedComment = commentToBookRepository.save(commentToBook);
        Book expectedBook = em.find(Book.class, bookOneId);
        assertThat(insertedComment.getBook().getId()).isEqualTo(expectedBook.getId());

        List<CommentToBook> comments = commentToBookRepository.getAllByBookId(bookOneId);
        assertThat(comments).hasSize(commentsCountForBook1 + 1)
                .contains(commentToBook);
    }

    @DisplayName("удаление комментария книги")
    @Test
    void removeCommentFromBook() {
        CommentToBook commentToBook = em.find(CommentToBook.class, commentToDeleteId);
        commentToBookRepository.delete(commentToBook);
        List<CommentToBook> comments = commentToBookRepository.getAllByBookId(bookOneId);
        assertThat(comments).hasSize(commentsCountForBook1 - 1)
                .doesNotContain(commentToBook);
    }
}
