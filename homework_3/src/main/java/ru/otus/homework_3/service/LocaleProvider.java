package ru.otus.homework_3.service;

import java.util.Locale;

public interface LocaleProvider {

    Locale getLocale();
}
