package ru.otus.homework_3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import ru.otus.homework_3.exception.ConsoleException;

@Service
public class MessageSourceServiceImpl implements MessageSourceService {

    private final MessageSource messageSource;
    private final InOutService inOutService;
    private final LocaleProvider localeProvider;

    @Autowired
    public MessageSourceServiceImpl(InOutService inOutService, MessageSource messageSource, LocaleProvider localeProvider) {
        this.inOutService = inOutService;
        this.messageSource = messageSource;
        this.localeProvider = localeProvider;
    }

    @Override
    public void printMessage(String message, String... messageArgs) {
        inOutService.printOutput(messageSource.getMessage(message, messageArgs, localeProvider.getLocale()));
    }

    @Override
    public void printOutput(String output) {
        inOutService.printOutput(output);
    }

    @Override
    public String readInput() throws ConsoleException {
        return inOutService.readInput();
    }
}
