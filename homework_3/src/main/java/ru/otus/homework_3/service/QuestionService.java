package ru.otus.homework_3.service;

import ru.otus.homework_3.domain.Question;
import ru.otus.homework_3.exception.QuestionsSourceException;

import java.util.List;

public interface QuestionService {
    List<Question> getContent() throws QuestionsSourceException;
}
