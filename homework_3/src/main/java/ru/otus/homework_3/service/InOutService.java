package ru.otus.homework_3.service;

import ru.otus.homework_3.exception.ConsoleException;
import ru.otus.homework_3.exception.QuestionsSourceException;

public interface InOutService {
    void printOutput(String output);

    String readInput() throws ConsoleException;
}
