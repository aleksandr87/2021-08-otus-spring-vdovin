package ru.otus.homework_3.service;

import org.springframework.stereotype.Component;
import ru.otus.homework_3.config.AppProperties;

@Component
public class ResourceProviderImpl implements ResourceProvider {

    private final AppProperties properties;

    ResourceProviderImpl(AppProperties properties) {
        this.properties = properties;
    }

    @Override
    public String getCsvFile() {
        return properties.getCsvFile();
    }
}
