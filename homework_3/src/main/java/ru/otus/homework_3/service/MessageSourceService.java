package ru.otus.homework_3.service;

public interface MessageSourceService extends InOutService {

    void printMessage(String message, String... messageArgs);
}
