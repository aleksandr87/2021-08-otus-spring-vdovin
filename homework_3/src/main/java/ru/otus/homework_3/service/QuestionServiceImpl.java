package ru.otus.homework_3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.otus.homework_3.dao.QuestionDao;
import ru.otus.homework_3.domain.Question;
import ru.otus.homework_3.exception.QuestionsSourceException;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {

    private final QuestionDao dao;

    @Autowired
    public QuestionServiceImpl(QuestionDao dao) {
        this.dao = dao;
    }

    @Override
    public List<Question> getContent() throws QuestionsSourceException {
        return dao.getAllQuestions();
    }
}
