package ru.otus.homework_3.service;

import org.springframework.stereotype.Component;
import ru.otus.homework_3.config.AppProperties;

import java.util.Locale;

@Component
public class LocaleProviderImpl implements LocaleProvider {

    private final AppProperties props;

    public LocaleProviderImpl(AppProperties props) {
        this.props = props;
    }

    @Override
    public Locale getLocale() {
        return Locale.forLanguageTag(props.getLanguage());
    }
}
