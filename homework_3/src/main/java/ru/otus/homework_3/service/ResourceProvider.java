package ru.otus.homework_3.service;

public interface ResourceProvider {

    String getCsvFile();
}
