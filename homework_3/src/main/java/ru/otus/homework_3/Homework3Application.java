package ru.otus.homework_3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import ru.otus.homework_3.config.AppProperties;
import ru.otus.homework_3.service.TestExecuteService;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class Homework3Application {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(Homework3Application.class, args);
        TestExecuteService service = context.getBean(TestExecuteService.class);
        service.executeTest();
    }
}
