package ru.otus.homework_3.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "app")
public class AppProperties {

    private static final String EXT = ".csv";

    private String language;
    private String csvFile;
    private int maxAllowedWrongAnswers;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCsvFile() {
        return csvFile.concat("_" + language + EXT);
    }

    public void setCsvFile(String csvFile) {
        this.csvFile = csvFile;
    }

    public int getMaxAllowedWrongAnswers() {
        return maxAllowedWrongAnswers;
    }

    public void setMaxAllowedWrongAnswers(int maxAllowedWrongAnswers) {
        this.maxAllowedWrongAnswers = maxAllowedWrongAnswers;
    }
}
