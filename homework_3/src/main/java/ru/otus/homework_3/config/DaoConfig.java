package ru.otus.homework_3.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import ru.otus.homework_3.dao.QuestionDao;
import ru.otus.homework_3.dao.QuestionDaoImpl;
import ru.otus.homework_3.service.ResourceProvider;

@Component
public class DaoConfig {

    private final ResourceProvider resourceProvider;

    public DaoConfig(ResourceProvider resourceProvider) {
        this.resourceProvider = resourceProvider;
    }

    @Bean
    public QuestionDao questionDao() {
        return new QuestionDaoImpl(resourceProvider);
    }
}
