package ru.otus.homework_3.enums;

public enum TestResultEnum {

    PASSED("PASSED"),
    NOT_PASSED("NOT PASSED");

    private final String resultMessage;

    private TestResultEnum(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    @Override
    public String toString() {
        return getResultMessage();
    }
}
