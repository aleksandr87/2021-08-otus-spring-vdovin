package ru.otus.homework_3.dao;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import ru.otus.homework_3.domain.Question;
import ru.otus.homework_3.exception.QuestionsSourceException;
import ru.otus.homework_3.service.ResourceProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class QuestionDaoImpl implements QuestionDao {
    private final ResourceProvider resourceProvider;

    public QuestionDaoImpl(ResourceProvider resourceProvider) {
        this.resourceProvider = resourceProvider;
    }

    @Override
    public List<Question> getAllQuestions() throws QuestionsSourceException {
        List<Question> questions = new ArrayList<>();
        Resource resource = new ClassPathResource(resourceProvider.getCsvFile());
        try (Scanner scanner = new Scanner(resource.getInputStream())) {
            while (scanner.hasNextLine()) {
                String[] arr = scanner.nextLine().split(";");
                String question = arr[0];
                List<String> answers = Arrays.asList(arr[1].split(":"));
                String rightAnswer = arr[2];
                questions.add(new Question(question, answers, rightAnswer));
            }
        } catch (IOException e) {
            throw new QuestionsSourceException(QuestionsSourceException.NOT_FOUND, resource.getFilename());
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new QuestionsSourceException(QuestionsSourceException.WRONG_FORMAT, resource.getFilename());
        }
        return questions;
    }
}
