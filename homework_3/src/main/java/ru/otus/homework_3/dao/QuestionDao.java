package ru.otus.homework_3.dao;

import ru.otus.homework_3.domain.Question;
import ru.otus.homework_3.exception.QuestionsSourceException;

import java.util.List;

public interface QuestionDao {
    List<Question> getAllQuestions() throws QuestionsSourceException;
}
